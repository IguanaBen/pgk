#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <time.h>

#define GLEW_STATIC

#include <GL/glew.h>

#include <GLFW/glfw3.h>
GLFWwindow* window;

#include <glm/glm.hpp>
using namespace glm;
using namespace std;

#include <common/shader.hpp>

int currentAnimation = 0; //0-none, 1-reveal, 2-show, 3-hide
double animationStartTime = 0.0;
const double flipTime = 0.4;
const double showTime = 1.0;
const double buttonCooldown = 0.2;

const unsigned int resX = 900;
const unsigned int resY = 700;

const int cardHeight = 150;
const int cardWidth = 145;
const int cardOutline = 10;
const int cardSpacing = 10;
const int boardStartX = 40;	//in pixels from lower left corner
const int boardStartY = 40;
const int selectionOutline = 5;
const int symbolSize = 140;

const int hudPosX = 700;
const int hudPosY = 50;
const int hudOffset = 10;
const int hudElementSize = 35;
const int maxHudColumnHeight = 10;

const int boardSize = 4;

vec3 cardFrontColor = vec3(1.0, 1.0, 1.0);
vec3 cardBackColor1 = vec3(0.0, 51.0/255.0, 102.0/255.0);
vec3 cardBackColor2 = vec3(51.0/255.0, 102.0/255.0, 153.0/255.0);
vec3 selectionColor = vec3(1.0, 1.0, 0.0);
vec3 backgroundColor = vec3(102.0/255.0,102.0/255.0,102.0/255.0);
vec3 boardLineColor = vec3(204.0/255.0,153.0/255.0,51.0/255.0);

vec3 hudColor = vec3(100.0/255.0, 255.0/255.0, 0.0/255.0);


const vec3 symbolColors[] = 
{
	vec3(1.0, 190.0/255.0, 0.0),
	vec3(110.0/255.0, 230.0/255.0, 42.0/255.0),
	vec3(172.0/255.0, 84.0/255.0, 170.0/255.0),
	vec3(251.0/255.0, 115.0/255.0, 116.0/255.0),
	vec3(197.0/255.0, 170.0/255.0, 245.0/255.0),
	vec3(226.0/255.0, 61.0/255.0, 128.0/255.0),
	vec3(19.0/255.0, 161.0/255.0, 203.0/255.0),
	vec3(153.0/255.0, 255.0/255.0, 0.0/255.0),
	vec3(157.0/255.0, 190.0/255.0, 42.0/255.0),
	vec3(172.0/255.0, 84.0/255.0, 170.0/255.0)
};


int selectedX = 0;
int selectedY = 0;

int prevSelectedX = 0;
int prevSelectedY = 0;
int prevSelectedX2 = 0;
int prevSelectedY2 = 0;

int currentRound = 0;
int revealState = 0;
int revealedCount = 0;
int cardState[boardSize][boardSize];
int cards[boardSize][boardSize];

void newGame()
{
	currentRound = 0;
	currentAnimation = 0;
	revealedCount = 0;

	prevSelectedX = -1;
	prevSelectedY = -1;
	prevSelectedX2 = -1;
	prevSelectedY2 = -1;

	revealedCount = 0;

	selectedX = 0;
	selectedY = boardSize-1;

	vector<int> cardIndices;
	for(int i=0; i<boardSize*boardSize; i++)
		cardIndices.push_back(i/2);

	random_shuffle(cardIndices.begin(), cardIndices.end());

	for(int i=0; i<boardSize; i++)
		for(int j=0; j<boardSize; j++)
		{
			cardState[i][j] = 0;
			cards[i][j] = cardIndices[i*boardSize + j];
		}
}


const unsigned int maxBufferVertsCount = 10000;  
GLfloat vertPositionBuffer[maxBufferVertsCount * 3];
GLfloat vertColorBuffer[maxBufferVertsCount * 3];

GLfloat linePositionBuffer[1000];
GLfloat lineColorBuffer[1000];
uint lineBufferSize;

GLfloat shapesBuffer[maxBufferVertsCount * 3];
GLfloat shapesColorBuffer[maxBufferVertsCount * 3];

// where shape starts in buffer
uint shapeIndices[50];
uint shapeSize[50];	//size of shape in buffer
uint totalShapeBufferSize;



// p1 - lower let, p2 - upper right,
// p1.z - depth, p2.z - ignored
void appendRectToBuffers(GLfloat *vertPosBuffer, GLfloat *vertColBuffer, 
						unsigned int &bufferSize, vec3 color, vec3 p1, vec3 p2 )
{


	double width = p2.x - p1.x;
	double height = p2.y - p1.y;
	

	vec3 transform = p1 - vec3(-width/2.0, -height/2.0, p1.z);

	GLfloat depth = p1.z;
	vec3 tri1[3], tri2[3];

	tri1[0] = vec3(-width/2.0, -height/2.0,depth) + transform;
	tri1[1] = vec3(width/2.0, -height/2.0, depth) + transform;
	tri1[2] = vec3(width/2.0, height/2.0, depth) + transform;

	tri2[0] = vec3(width/2.0, height/2.0, depth) + transform;
	tri2[1] = vec3(-width/2.0, height/2.0, depth) + transform;
	tri2[2] = vec3(-width/2.0, -height/2.0,depth) + transform;

	for(int i=0; i<3; i++)
	{	
		vertColBuffer[bufferSize] = color.x;
		vertPosBuffer[bufferSize++] = tri1[i].x;
		vertColBuffer[bufferSize] = color.y;
		vertPosBuffer[bufferSize++] = tri1[i].y;
		vertColBuffer[bufferSize] = color.z;
		vertPosBuffer[bufferSize++] = tri1[i].z;
	}

	for(int i=0; i<3; i++)
	{
		vertColBuffer[bufferSize] = color.x;
		vertPosBuffer[bufferSize++] = tri2[i].x;
		vertColBuffer[bufferSize] = color.y;
		vertPosBuffer[bufferSize++] = tri2[i].y;
		vertColBuffer[bufferSize] = color.z;
		vertPosBuffer[bufferSize++] = tri2[i].z;
	}

}

//around center of the axis
vec3 rotateZ(vec3 p, double rads)
{	
	return vec3(p.x*cos(rads) - p.y*sin(rads), p.x*sin(rads) + p.y*cos(rads), p.z );
}

vec3 scale(vec3 p, vec3 scale)
{
	return vec3(p.x * scale.x , p.y * scale.y , p.z * scale.z);
}

// 0-triangle, 1-reversed triangle, 2-rectangle, 3-deltoid
// 4-plus, 5-star, 6-circle(somewhat), 7-hexagon, 8 - star
//p1, p2 - boundaries
void addShapeToBuffers(GLfloat *vertPosBuffer, GLfloat *vertColBuffer, 
						unsigned int &bufferSize, vec3 color, vec3 p1, vec3 p2,
						unsigned int shape, double baseRotationZ)
{

	double width = p2.x - p1.x;
	double height = p2.y - p1.y;
	GLfloat depth = p1.z;

	vec3 transform = p1 - vec3(-width/2.0, -height/2.0, p1.z);
	double rotationZ = baseRotationZ;

	if(shape == 4 || shape == 5 || shape > 7)
	{
		if(shape == 5) rotationZ += M_PI_4;

		int sh = 2;
		if(shape > 7) sh = 3;

		addShapeToBuffers(vertPosBuffer, vertColBuffer, bufferSize, color,
						vec3(-width/10.0, -height/2.0, p1.z) + transform,
						vec3(width/10.0, height/2.0, p2.z) + transform, sh, rotationZ );
		addShapeToBuffers(vertPosBuffer, vertColBuffer, bufferSize, color,
						vec3(-width/2.0, -height/10.0, p1.z) + transform,
						vec3(width/2.0, height/10.0, p2.z) + transform, sh, rotationZ );
		if(shape > 8)
		{
			rotationZ += M_PI_4;

			addShapeToBuffers(vertPosBuffer, vertColBuffer, bufferSize, color,
							vec3(-width/10.0, -height/2.0, p1.z) + transform,
							vec3(width/10.0, height/2.0, p2.z) + transform, sh, rotationZ );
			addShapeToBuffers(vertPosBuffer, vertColBuffer, bufferSize, color,
							vec3(-width/2.0, -height/10.0, p1.z) + transform,
							vec3(width/2.0, height/10.0, p2.z) + transform, sh, rotationZ );
		}
	}
	else
	{

		int steps = 60;
		if(shape == 7) steps = 5;
		if(shape <=1 ) steps = 3;
		if(shape == 1) rotationZ += M_PI;

		if(shape == 2 || shape == 3 ) steps = 4;
		// if(shape == 3) rotationZ += M_PI_4;

		double angle = -2.0*M_PI/steps;
		vec3 center = vec3(0.0, 0.0, p1.z);
		vec3 p1 = vec3(0.0, height/2.0, 0.0);
		vec3 p2 = rotateZ(p1, angle);

		if( shape == 2 )
		{
			p1 = rotateZ(p1, M_PI_4);
			p2 = rotateZ(p2, M_PI_4);
		}

		double ratio = width/height;

		for(int i=0; i<steps; i++)
		{
			vec3 tri[3];

			tri[0] = rotateZ(scale( p1, vec3(ratio, 1, 1)), rotationZ)  + transform;
			tri[1] = rotateZ(scale( center,  vec3(ratio, 1, 1)), rotationZ)   + transform;;
			tri[2] = rotateZ(scale( p2, vec3(ratio, 1, 1)), rotationZ)   + transform;

			for(int i=0; i<3; i++)
			{	
				vertColBuffer[bufferSize] = color.x;
				vertPosBuffer[bufferSize++] = tri[i].x;
				vertColBuffer[bufferSize] = color.y;
				vertPosBuffer[bufferSize++] = tri[i].y;
				vertColBuffer[bufferSize] = color.z;
				vertPosBuffer[bufferSize++] = tri[i].z;
			}
			p1 = rotateZ(p1, angle); 
			p2 = rotateZ(p2, angle); 
		}
	}
}

void initBuffers(GLuint shapebuff, GLuint shapecolbuff, GLuint  linebuffer, GLuint linecolorbuffer)
{	
	totalShapeBufferSize = 0;

	// vec3 symbolP1 = vec3(-2.0*(symbolSize)/resX - 1.0f, -2.0/resY - 1.0f ,  0.0);

	// vec3 symbolP2 = vec3(symbolP1.x + float(2*symbolSize)/resX, symbolP1.y + float(2*symbolSize)/resX,  0.0);



	vec3 symbolP1 = vec3(-float(symbolSize)/resX, -float(symbolSize)/resX ,  0.0);
	vec3 symbolP2 = vec3(symbolP1.x + float(2*symbolSize)/resX, symbolP1.y + float(2*symbolSize)/resX,  0.0);

	for(int i=0; i <= 9; i++)	//shapes on card
	{
		// uint prevShapeStart = currentShapeStart;
		shapeIndices[i] = totalShapeBufferSize;
		addShapeToBuffers(shapesBuffer, shapesColorBuffer, totalShapeBufferSize, 
							symbolColors[i], symbolP1, symbolP2, i, 0);

		shapeSize[i] = totalShapeBufferSize - shapeIndices[i];
	}

	vec3 p1 = vec3(-float(cardWidth)/resX, -float(cardHeight)/resY ,  0.0);
	vec3 p2 = vec3(p1.x + float(2*cardWidth)/resX, p1.y + float(2*cardHeight)/resY,  0.0);

	// front of the card
	shapeIndices[10] = totalShapeBufferSize;
	appendRectToBuffers(shapesBuffer, shapesColorBuffer, totalShapeBufferSize, 
					cardFrontColor, p1, p2);
	shapeSize[10] = totalShapeBufferSize - shapeIndices[10];


	//back of the card
	shapeIndices[11] = totalShapeBufferSize;
	appendRectToBuffers(shapesBuffer, shapesColorBuffer, totalShapeBufferSize, 
					cardBackColor1, p1, p2);

	appendRectToBuffers(shapesBuffer, shapesColorBuffer, totalShapeBufferSize, 
					cardBackColor2, p1 + vec3( float(2*cardOutline)/resX, float(2*cardOutline)/resY, 0.0), 
					p2 - vec3( float(2*cardOutline)/resX, float(2*cardOutline)/resY, 0.0));
	shapeSize[11] = totalShapeBufferSize - shapeIndices[11];


	// selection
	p1 = vec3(-float(cardWidth + 2*selectionOutline)/resX, -float(cardHeight + 2*selectionOutline)/resY ,  0.0);
	p2 = vec3(p1.x + float(2*(cardWidth + 2*selectionOutline))/resX, 
			p1.y + float(2*(cardHeight + 2*selectionOutline))/resY,  0.0);

	shapeIndices[12] = totalShapeBufferSize;

	appendRectToBuffers(shapesBuffer, shapesColorBuffer, totalShapeBufferSize, 
					selectionColor, p1, p2);

	shapeSize[12] = totalShapeBufferSize - shapeIndices[12];


	//lines
	lineBufferSize = 0;
	for(int i=1; i<boardSize; i++)
	{
		vec3 pp[4];
		pp[0] = vec3(2.0*(boardStartX + i*(cardWidth + cardSpacing) - selectionOutline)/resX - 1.0f, 
				2.0*(boardStartY  -15)/resY - 1.0f,  0.0);

		pp[1] = vec3(2.0*(boardStartX + i*(cardWidth + cardSpacing) - selectionOutline )/resX - 1.0f, 
				2.0*(boardStartY +20 + boardSize*(cardWidth + cardSpacing))/resY - 1.0f,  0.0);

		pp[2] = vec3(2.0*(boardStartX -15)/resX - 1.0f, 
				2.0*(boardStartY + i*(cardWidth + cardSpacing + 5) - selectionOutline)/resY - 1.0f,  0.0);

		pp[3] = vec3(2.0*(boardStartX + boardSize*(cardWidth + cardSpacing) +10)/resX - 1.0f, 
				2.0*(boardStartY+ i*(cardWidth + cardSpacing + 5) - selectionOutline)/resY - 1.0f,  0.0);

		for(int k=0; k<4; k++)
		{
			// cout<<pp[0].x<<" "<<pp[0].y<<" "<<pp[1].x<<" "<<pp[1].y<<"\n";
			lineColorBuffer[lineBufferSize] = boardLineColor.x;
			linePositionBuffer[lineBufferSize++] = pp[k].x;
			lineColorBuffer[lineBufferSize] =  boardLineColor.y;
			linePositionBuffer[lineBufferSize++] = pp[k].y;
			lineColorBuffer[lineBufferSize] =  boardLineColor.z;
			linePositionBuffer[lineBufferSize++] = pp[k].z;
		}
	}
	
	glBindBuffer(GL_ARRAY_BUFFER, linebuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * lineBufferSize, linePositionBuffer, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, linecolorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * lineBufferSize, lineColorBuffer, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, shapebuff);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * totalShapeBufferSize, shapesBuffer, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, shapecolbuff);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * totalShapeBufferSize, shapesColorBuffer, GL_STATIC_DRAW);

	glLineWidth(8.0);

}

void drawCard(int i, int j, unsigned int &bufferSize, GLuint transformLocation, GLuint scaleLocation)
{

	double scaleX = 1.0f;

	bool isAnimated = false;
	
	if(currentAnimation == 1 && revealState == 1)
		isAnimated = (i == prevSelectedX && j == prevSelectedY);

	if(currentAnimation == 1 && revealState == 2)
		isAnimated = (i == prevSelectedX2 && j == prevSelectedY2);

	if(currentAnimation == 3)
		isAnimated = (i == prevSelectedX && j == prevSelectedY)||(i == prevSelectedX2 && j == prevSelectedY2);

	isAnimated = isAnimated && currentAnimation;
	if( isAnimated && (currentAnimation == 1 || currentAnimation == 3))
		scaleX = cos(M_PI*(glfwGetTime() - animationStartTime)/flipTime);

	bool halfTime = (glfwGetTime() - animationStartTime >= flipTime/2.0);



	GLfloat posX = 2.0*(boardStartX + cardWidth*0.5 + i*(cardWidth + cardSpacing))/resX - 1.0;
	GLfloat posY = 2.0*(boardStartY + cardHeight*0.5 + j*(cardHeight + cardSpacing) )/resY - 1.0f;
	vec3 transform = vec3(posX, posY, 0.0f);
	glUniform3fv(transformLocation, 1, &transform[0]);

	vec3 scale = vec3(scaleX, 1.0, 1.0);
	glUniform3fv(scaleLocation, 1, &scale[0]);

	//unturned or turngin
	if(cardState[i][j] == 0 || (currentAnimation == 1 && !halfTime && isAnimated)
							|| (currentAnimation == 3 && halfTime && isAnimated))
	{
		glDrawArrays(GL_TRIANGLES, shapeIndices[11]/3, shapeSize[11]/3);
	}
	else
	{
		glDrawArrays(GL_TRIANGLES, shapeIndices[10]/3, shapeSize[10]/3);

		int symbolIndex = cards[i][j];
		if(symbolIndex == 2) symbolIndex = 9;
		if(symbolIndex == 1) symbolIndex = 8;

		glDrawArrays(GL_TRIANGLES, shapeIndices[symbolIndex]/3, shapeSize[symbolIndex]/3);
	}
}

void drawUpdateScene(GLuint vertexbuffer, GLuint colorbuffer, 
					GLuint linebuffer, GLuint linecolorbuffer, 
					GLuint shapebuff, GLuint shapecolbuff,
					GLuint transformLocation, GLuint scaleLocation,

					GLuint programID)
{
	unsigned int bufferSize = 0;

	if( glfwGetTime() -  animationStartTime > flipTime && currentAnimation == 1)
	{
		if(revealState == 2)
		{
			currentRound++;
			if( cards[prevSelectedX][prevSelectedY] != cards[prevSelectedX2][prevSelectedY2] )
			{
				// cout<<"no\n";
				currentAnimation = 2;
			}
			else
			{
				revealedCount++;
				if(revealedCount*2 == boardSize*boardSize)
					currentAnimation = 2;
				else
					currentAnimation = 0;
			}

			animationStartTime = glfwGetTime();
			revealState = 0;
		}
		else
		{
			currentAnimation = 0;
		}
	}

	if( glfwGetTime() -  animationStartTime > showTime && currentAnimation == 2)
	{
		if(revealedCount*2 == boardSize*boardSize)
			newGame();

		animationStartTime = glfwGetTime();
		currentAnimation = 3;
	}

	if( glfwGetTime() -  animationStartTime > flipTime && currentAnimation == 3)
	{
		animationStartTime = glfwGetTime();
		cardState[prevSelectedX][prevSelectedY] = 0;
		cardState[prevSelectedX2][prevSelectedY2] = 0;
		currentAnimation = 0;
	}

	glClear( GL_COLOR_BUFFER_BIT );

	glUseProgram(programID);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	vec3 transform = vec3(0.0f, 0.0f, 0.0f);
	vec3 scale = vec3(1.0f, 1.0f, 1.0f);
	glUniform3fv(transformLocation, 1, &transform[0]);
	glUniform3fv(scaleLocation, 1, &scale[0]);


	glBindBuffer(GL_ARRAY_BUFFER, linebuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0	);

	glBindBuffer(GL_ARRAY_BUFFER, linecolorbuffer);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0	);

	glDrawArrays(GL_LINES, 0, lineBufferSize/3);


	glBindBuffer(GL_ARRAY_BUFFER, shapebuff);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0	);

	glBindBuffer(GL_ARRAY_BUFFER, shapecolbuff);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0	);

	//draw selection
	GLfloat posX = 2.0*(boardStartX + cardWidth*0.5 + selectedX*(cardWidth + cardSpacing))/resX - 1.0;
	GLfloat posY = 2.0*(boardStartY + cardHeight*0.5 + selectedY*(cardHeight + cardSpacing) )/resY - 1.0f;
	transform = vec3(posX, posY, 0.0f);
	scale = vec3(1.0f, 1.0f, 1.0f);
	glUniform3fv(transformLocation, 1, &transform[0]);
	glUniform3fv(scaleLocation, 1, &scale[0]);
	glDrawArrays(GL_TRIANGLES, shapeIndices[12]/3, shapeSize[12]/3);


	for(int i=0; i<boardSize; i++)
		for(int j=0; j<boardSize; j++)
			drawCard(i,j, bufferSize, transformLocation, scaleLocation);



	int ccolumn = 0;
	int col = 0;
	scale = vec3(float(hudElementSize)/cardWidth, float(hudElementSize)/cardHeight, 1.0f);
	glUniform3fv(scaleLocation, 1, &scale[0]);

	for(int i=0; i<currentRound; i++)
	{
		transform = vec3(2.0*(hudPosX+ col*(hudElementSize*0.5 + 2.0*hudOffset))/resX - 1.0f,
						 2.0*(hudPosY + (i - col*maxHudColumnHeight)*(hudElementSize*0.5 + hudOffset))/resY - 1.0f,
						 0.0f);

		glUniform3fv(transformLocation, 1, &transform[0]);
		glDrawArrays(GL_TRIANGLES, shapeIndices[1]/3, shapeSize[1]/3);


		ccolumn++;
		if(ccolumn >= maxHudColumnHeight)
		{
			col++;
			ccolumn = 0;
		}
	}	

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	glfwSwapBuffers(window);

}

void handle_input(bool &doExit, double currentTime)
{
	glfwPollEvents();

	if(glfwGetKey(window, GLFW_KEY_ESCAPE ) == GLFW_PRESS)
		doExit = true;

	static double lastPressedUp = -buttonCooldown;
	if(glfwGetKey(window, GLFW_KEY_UP ) == GLFW_RELEASE) lastPressedUp = -buttonCooldown;

	if(glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS 
		&& currentTime - lastPressedUp >= buttonCooldown)
	{
		lastPressedUp = currentTime;
		selectedY = (selectedY + 1) % boardSize;
	}

	static double lastPressedDown = -buttonCooldown;
	if(glfwGetKey(window, GLFW_KEY_DOWN ) == GLFW_RELEASE) lastPressedDown = -buttonCooldown;

	if(glfwGetKey(window, GLFW_KEY_DOWN ) == GLFW_PRESS 
		&& currentTime - lastPressedDown >= buttonCooldown)
	{
		lastPressedDown = currentTime;
		selectedY = (selectedY + 3) % boardSize;

	}

	static double lastPressedLeft = -buttonCooldown;
	if(glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_RELEASE) lastPressedLeft = -buttonCooldown;

	if(glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS 
		&& currentTime - lastPressedLeft >= buttonCooldown)
	{
		lastPressedLeft = currentTime;
		selectedX = (selectedX + 3) % boardSize;

	}

	static double lastPressedRight = -buttonCooldown;
	if(glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_RELEASE) lastPressedRight = -buttonCooldown;

	if(glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS 
		&& currentTime - lastPressedRight >= buttonCooldown)
	{
		lastPressedRight = currentTime;
		selectedX = (selectedX + 1) % boardSize;

	}


	static double spacedPressed = false;
	if(glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_RELEASE) spacedPressed = false;

	if(glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_PRESS && !spacedPressed && currentAnimation==0)
	{
		spacedPressed = true;
		if(cardState[selectedX][selectedY] == 0)
		{
			cardState[selectedX][selectedY] = 1;

			if(revealState == 0)
			{
				prevSelectedX = selectedX;
				prevSelectedY = selectedY;
			}
			else
			{
				prevSelectedX2 = selectedX;
				prevSelectedY2 = selectedY;
			}

			revealState++;
			currentAnimation = 1;
			animationStartTime = glfwGetTime();
		}
		// cout<<"space\n";
	}

}

int main( void )
{
	srand(time(NULL));

	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow( resX, resY, "Memory", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glewExperimental = true;
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, 0.0f);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	GLuint programID = LoadShaders( "memory_vs.vertexshader", "memory_fs.fragmentshader" );

	GLuint vertexbuffer;
	GLuint colorbuffer;
	GLuint linebuffer;
	GLuint linecolorbuffer;

	GLuint shapeBuff;
	GLuint shapeBuffCol;

	glGenBuffers(1, &shapeBuff);
	glGenBuffers(1, &shapeBuffCol);

	glGenBuffers(1, &vertexbuffer);
	glGenBuffers(1, &colorbuffer);
	glGenBuffers(1, &linebuffer);
	glGenBuffers(1, &linecolorbuffer);

	initBuffers(shapeBuff, shapeBuffCol, linebuffer, linecolorbuffer);

	GLuint transformLocation = glGetUniformLocation(programID, "transformVector");
	GLuint scaleLocation = glGetUniformLocation(programID, "scaleVector");


	newGame();
	bool doExit = false;

	do{
		double currentTime = glfwGetTime();


		drawUpdateScene(vertexbuffer, colorbuffer, linebuffer,
						linecolorbuffer, shapeBuff, shapeBuffCol,
						transformLocation, scaleLocation, programID);

		handle_input(doExit, currentTime);
	}
	while( !doExit && glfwWindowShouldClose(window) == 0 );

	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &colorbuffer);
	glDeleteBuffers(1, &linebuffer);
	glDeleteBuffers(1, &linecolorbuffer);

	glDeleteVertexArrays(1, &VertexArrayID);
	glDeleteProgram(programID);

	glfwTerminate();

	return 0;
}

