#ifndef _TEXT_RENDERER_H_
#define _TEXT_RENDERER_H_

#include <stddef.h>
#include <string>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <algorithm>
#include <vector>

#include <common/shader.hpp>
#include "texture_loader.h"

class text_renderer
{
	public:
		text_renderer();
		void draw(std::string text, GLfloat x, GLfloat y, GLfloat size, GLfloat span);

	private:
		GLuint fontProgram;
		GLuint font_texture;
		GLuint texture_id;
		GLfloat vertices;
		GLuint vs_position;
		GLuint vertexUVID;

		GLuint vertexbuffer;
		GLuint uvbuffer;
		GLuint MatrixID;
};

#endif