/*
Michał Gańczorz

sterowanie: 
	WSAD - kierunki,
	SHIFT - dół, 
	SPACJA -góra,

	Alternatywnie:
	Strzałki - kierunki,
	PgUp - dół, 
	PgDn -góra,

	Kąt widzenia(zoom) - kółko myszy

	mysz-kamera(w widoku także obrót wdłg osi Y(pionowej))
*/

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <time.h>
#include <list>
#include <set>


#define GLEW_STATIC

#include <GL/glew.h>

#include <GLFW/glfw3.h>
GLFWwindow* window;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

using namespace glm;
using namespace std;

#include <common/shader.hpp>
#include "text_renderer.h"

const uint resX = 1200;
const uint resY = 700;

const double deltaTime = 0.01666;

// vec3 paddleColor = vec3(108.0/255.0, 123.0/255.0, 139.0/255.0);
// vec3 blockOutlineColor = vec3(0.0, 0.0, 0.0);
vec3 backgroundColor = vec3(0.65f, 0.65f, 0.1f);
vec3 boardColor = vec3(0.65f, 0.65f, 0.1f);
vec3 wallsColor = vec3(0.1f, 0.1f, 0.60f);

vec4 portalColor = vec4(0.05f, 0.9f, 0.05f, 1.0f);
vec4 waterColor = vec4(0.4f, 0.6f, 1.0f, 1.0f);
vec4 boxColor = vec4(0.4f, 0.4f, 1.0f, 1.0f);
// vec3 backgroundLineColor = vec3(1.0/255.0, 1.0/255.0, 140.0/255.0);
// vec3 boardBoardersColor = vec3(1.0/255.0, 1.0/255.0, 100.0/255.0);
// vec3 ballColor = vec3(207.0/255.0, 10.0/255.0, 42.0/255.0);

GLuint vertexbuffer;

const vec3 playerColor = vec3(1.0, 1.0, 0.0);
// const vec3 playerColor = vec3(102.0/255.0, 51.0/255.0, 153.0/255.0);

const vec3 specialBubbleColor = vec3(1.0, 0.0, 0.0);
const vec3 bubbleColors[] =
{
	vec3(1.0, 190.0/255.0, 0.0),
	// vec3(110.0/255.0, 230.0/255.0, 42.0/255.0),
	vec3( 172.0/255, 0.0/255.0, 170.0/255.0),
	vec3(0.0/255.0, 0.0/255.0, 116.0/255.0),
	vec3( 197.0/255.0, 50.0/255.0,  245.0/255.0),
	vec3( 61.0/255.0, 226.0/255.0, 128.0/255.0),
	// vec3(19.0/255.0, 161.0/255.0, 203.0/255.0),
	// vec3(153.0/255.0, 255.0/255.0, 0.0/255.0),
	vec3( 1.0/255.0, 157.0/255.0, 42.0/255.0),
	vec3(84.0/255.0, 172.0/255.0,  170.0/255.0)
};

const uint sphereResX = 20;
const uint sphereResY = 20;

const GLfloat sphereRadius = 1.0;
#define BOX 0

uint modelIndex[100];
uint modelSize[100];

const uint maxBufferVertsCount = 10000;
GLfloat modelBuffer[maxBufferVertsCount * 3];
GLfloat modelNormalBuffer[maxBufferVertsCount * 3];

GLfloat colorBuffer[maxBufferVertsCount * 3];

uint sphereBufferSize = 0;
GLfloat sphereBuffer[maxBufferVertsCount * 3];

uint shapeBufferSize = 0;
uint colorBufferSize = 0;

uint modelNormalBufferSize = 0;

uint modelBufferSize = 0;

uint colorIndexStart[100];
uint colorSize[100];

double mouseMoveX = 0.0f;
double mouseMoveY = 0.0f;
double mouseScroll = 0.0f;

bool pressedKeys[456];

GLuint sphereBufferID;
GLuint modelBufferID;
GLuint modelNormalBufferID;
GLuint lightsUniformBufferID;

GLuint lightsBindingPoint = 1;
struct light
{
    vec3 position;
   	int lightType;
    vec3 ambient;
    int isEnabled;
    vec3 diffuse;
    int isDistanceLight; //czy jest zalezne od odleglosci
    vec3 specular;
    int specularPower;
};

const int maxLightCount = 20;
light lights[30];


struct sphereProgramNfo
{
	GLuint sphereProgramID;
	GLuint colorLocation;
	GLuint MVPMatrixLocation;
	GLuint MMatrixLocation;
	// GLuint VMatrixLocation;
	GLuint normalMatrixLocation;
	GLuint camPosLocation;
	GLuint boxLimitLocation;
	GLuint drawIlluminatingLocaiton;
};

struct modelProgramNfo
{
	GLuint modelProgramID;
	GLuint colorLocation;
	GLuint MVPMatrixLocation;
	// GLuint VMatrixLocation;
	GLuint MMatrixLocation;
	// GLuint MVMatrixLocation;
	GLuint normalMatrixLocation;
	GLuint camPosLocation;
	GLuint drawWaterLocation;
	GLuint cameraModeLocation;

};

struct bubble
{
	vec4 bubbleColor;
	vec3 position;
	float speed;
	bool isSpecial;
	float radius;
	float growthRatio;
	int lightIndex;
};

class bubbleCmp
{
	vec3 pt;

	public:
		bubbleCmp(vec3 pt)
		{
			this->pt = pt;
		}

		bool operator() (const bubble &b1, const bubble &b2)
		{
			return length(pt-b1.position) > length(pt-b2.position);
		}
};

struct gameConfig
{
	float boxSize;
	float playerSize;
	float playerAcceleration;
	float playerDeceleration;

	float playerMaxSpeed;

	float minBubbleSize;
	float maxBubbleSize;
	float bubbleGrowthRatio;
	float bubbleGrowthPerLevel;

	int defaultMaxBubbles;
	float defautSpawnPpb;
	int specialBubbleLimit;
	float specialSpawnPpb;

	float minBubbleSpeed;
	float maxBubbleSpeed;

	// gameConfig(float boxSize, float playerSize, float playerAcceleration, float playerDeceleration,
	// 			float playerMaxSpeed, float minimumBubbleSize, float maximumBubbleSize,
	// 			float bubbleGrowthRatio, float bubbleGrowthPerLevel, float defautPpb,
	// 			float specialSpawnPpb, int defaultMaxBubbles)
	gameConfig()
	{
		this->boxSize = 20.0f;
		this->playerSize = 1.0f;
		this->playerAcceleration = 4.0f;
		this->playerDeceleration = 1.5f;

		this->playerMaxSpeed = 2.0f;

		this->minBubbleSize = 0.6f;
		this->maxBubbleSize = 1.8f;
		this->bubbleGrowthRatio = 0.20f;
		this->bubbleGrowthPerLevel = 1.4f;
		this->defautSpawnPpb = 0.03f;
		this->specialSpawnPpb = 0.1f;
		this->defaultMaxBubbles = 10;

		this->minBubbleSpeed = 0.3;
		this->maxBubbleSpeed = 2.0f;
		specialBubbleLimit = 20;
	}
};

string intToString(int x)
{
   stringstream ss;
   ss << x;
   return ss.str();
}

double randomPS(double maxval)
{
	return maxval*(double(rand() % 100000)/100000.0);
}

struct gameState
{
	int cameraMode;
	vec3 playerPosition;
	vec3 playerSpeed;
	vec3 playerDirection;
	vec3 portalLocation;
	vec3 portalScale;
	unsigned int currentLevel;
	unsigned int points;
	vec3 FPSCamDirection;
	vec3 camPosition;
	vec3 camDirection;
	float FPPzoom;
	float OUTzoom;
	float FPPpitch;
	float OUTpitch;
	float OUTyaw;


	unsigned int state;

	int specialBubbleCount;
	list<bubble> bubbles;
	bubble playerBubble;
};

void reset(gameState &gSt,gameConfig gConfig) 
{
	gSt.cameraMode = 0;
	gSt.playerPosition = vec3(0.0f, 0.0f, -gConfig.boxSize/2.0 + 2.0*gConfig.playerSize);
	gSt.playerSpeed = vec3(0.0f, 0.0f, 0.0f);
	gSt.playerDirection = vec3(0.0f, 0.0f, 1.0f);
	gSt.FPSCamDirection = vec3(0.0f, 0.0f, 1.0f);
	gSt.camPosition = gSt.playerPosition;
	gSt.camDirection = gSt.playerDirection;
	gSt.FPPzoom = 1.0f;
	gSt.FPPpitch = 0.0f;
	gSt.OUTzoom = 1.0f;
	gSt.OUTpitch = 0.0f;
	gSt.OUTyaw = 0.0f;
	gSt.specialBubbleCount = 0;
	gSt.points = 0;
	gSt.currentLevel = 1;

	gSt.portalLocation = vec3( randomPS(gConfig.boxSize/2.0f) - gConfig.boxSize/2.0f,
							   randomPS(gConfig.boxSize/2.0f) - gConfig.boxSize/2.0f,
							    gConfig.boxSize/2.0f +gConfig.playerSize/4.0f);

	float sc =gConfig.boxSize/(3.0f * (1.0f+gSt.currentLevel*0.2f));
	if(sc < gConfig.playerSize*2.0f ) sc = gConfig.playerSize*2.0f;
	gSt.portalScale = vec3(sc, sc, gConfig.playerSize/2.0f);


	gSt.bubbles.clear();
	gSt.state = 1;

	gSt.playerBubble.radius = gConfig.playerSize/2.0f; 
	gSt.playerBubble.bubbleColor = vec4(playerColor, 1.0f);
	gSt.playerBubble.growthRatio = 0.0f;
	gSt.playerBubble.isSpecial = true;
	for(int i=2; i<maxLightCount; i++) lights[i].isEnabled = false;

}

void nextLevel(gameState &gSt, gameConfig gConfig)
{
	// gSt.cameraMode = 0;
	gSt.playerPosition = vec3(0.0f, 0.0f, -gConfig.boxSize/2.0 + 2.0*gConfig.playerSize);
	gSt.playerSpeed = vec3(0.0f, 0.0f, 0.0f);
	gSt.playerDirection = vec3(0.0f, 0.0f, 1.0f);
	gSt.FPSCamDirection = vec3(0.0f, 0.0f, 1.0f);
	gSt.camPosition = gSt.playerPosition;
	gSt.camDirection = gSt.playerDirection;
	gSt.FPPzoom = 1.0f;
	gSt.FPPpitch = 0.0f;
	gSt.OUTzoom = 1.0f;
	gSt.OUTpitch = 0.0f;
	gSt.OUTyaw = 0.0f;
	gSt.specialBubbleCount = 0;
	gSt.points += 10*gSt.currentLevel;
	gSt.currentLevel++;

	gSt.portalLocation = vec3( randomPS(gConfig.boxSize/2.0f) - gConfig.boxSize/4.0f,
							   randomPS(gConfig.boxSize/2.0f) - gConfig.boxSize/4.0f,
							    gConfig.boxSize/2.0f +gConfig.playerSize/4.0f);

	float sc =gConfig.boxSize/(3.0f * (1.0f+gSt.currentLevel*0.2f));
	if(sc < gConfig.playerSize*2.0f ) sc = gConfig.playerSize*2.0f;
	gSt.portalScale = vec3(sc, sc, gConfig.playerSize/2.0f);

	gSt.bubbles.clear();
	gSt.state = 1;

	gSt.playerBubble.radius = gConfig.playerSize/2.0f; 
	gSt.playerBubble.bubbleColor = vec4(playerColor, 1.0f);
	gSt.playerBubble.growthRatio = 0.0f;
	gSt.playerBubble.isSpecial = true;
	for(int i=2; i<maxLightCount; i++) lights[i].isEnabled = false;
}



sphereProgramNfo loadSphereProgram()
{
	sphereProgramNfo nfo;
	nfo.sphereProgramID = LoadShaders( "bubbles_sphere_vs.vertexshader", "bubbles_sphere_fs.fragmentshader" );
	nfo.colorLocation = glGetUniformLocation(nfo.sphereProgramID, "sphereColor");
	nfo.MVPMatrixLocation = glGetUniformLocation(nfo.sphereProgramID, "MVP");
	// nfo.VMatrixLocation = glGetUniformLocation(nfo.sphereProgramID, "V");
	nfo.MMatrixLocation = glGetUniformLocation(nfo.sphereProgramID, "M");
	nfo.normalMatrixLocation = glGetUniformLocation(nfo.sphereProgramID, "NM");
	nfo.camPosLocation = glGetUniformLocation(nfo.sphereProgramID, "camPos");
	nfo.boxLimitLocation = glGetUniformLocation(nfo.sphereProgramID, "boxLimit");
	nfo.drawIlluminatingLocaiton = glGetUniformLocation(nfo.sphereProgramID, "drawIlluminating");

	GLuint uniformBlockIndex = glGetUniformBlockIndex(nfo.sphereProgramID, "lights_block");
	glUniformBlockBinding(nfo.sphereProgramID, uniformBlockIndex, lightsBindingPoint);

	return nfo;
}

modelProgramNfo loadModelProgram()
{
	modelProgramNfo nfo;
	nfo.modelProgramID = LoadShaders( "bubbles_model_vs.vertexshader", "bubbles_model_fs.fragmentshader" );
	nfo.colorLocation = glGetUniformLocation(nfo.modelProgramID, "modelColor");
	nfo.MVPMatrixLocation = glGetUniformLocation(nfo.modelProgramID, "MVP");
	// nfo.MVMatrixLocation = glGetUniformLocation(nfo.modelProgramID, "MV");
	// nfo.VMatrixLocation = glGetUniformLocation(nfo.modelProgramID, "V");
	nfo.MMatrixLocation = glGetUniformLocation(nfo.modelProgramID, "M");
	nfo.normalMatrixLocation = glGetUniformLocation(nfo.modelProgramID, "NM");
	nfo.camPosLocation = glGetUniformLocation(nfo.modelProgramID, "camPos");
	nfo.drawWaterLocation = glGetUniformLocation(nfo.modelProgramID, "drawWater");
	nfo.cameraModeLocation = glGetUniformLocation(nfo.modelProgramID, "cameraMode");

	GLuint uniformBlockIndex = glGetUniformBlockIndex(nfo.modelProgramID, "lights_block");
	glUniformBlockBinding(nfo.modelProgramID, uniformBlockIndex, lightsBindingPoint);

	return nfo;
}

void viewProjectionMatrices( gameState *gState, gameConfig *gConfig, mat4 &viewMatrix, mat4 &projMatrix)
{

	float FOV;

	if(gState->cameraMode == 0)
		FOV = 60.0f * gState->FPPzoom;
	else
		FOV = 60.0f * gState->OUTzoom;

	projMatrix = glm::perspective(FOV, resX / float(resY), gConfig->playerSize/2.0f - 0.1f, 100.0f);
	if(gState->cameraMode == 0)
	{
		float pitchAngle = M_PI*gState->FPPpitch*0.6;
		viewMatrix = glm::lookAt( gState->camPosition,
									gState->camDirection + 
								   vec3(0.0,  sin(pitchAngle), 0.0) + gState->camPosition, 
								  glm::vec3(0,1,0));
	}
	else
	{
		// cout<<gState->OUTyaw<<"\n";
		float pitchAngle = M_PI*gState->OUTpitch*0.2;
		float yawAngle =  4.0f*M_PI*gState->OUTyaw;
		vec3 dir = gState->playerPosition - gState->camPosition;
		// vec3 dir = rotateY( vec3(0.0f, sin(pitchAngle), 0.0f), yawAngle);
		dir = rotateY(dir, yawAngle);
		dir = normalize(dir);
		dir.y += sin(pitchAngle);
		viewMatrix = glm::lookAt( gState->camPosition,
					// gState->camDirection + 
					dir + gState->camPosition,
					glm::vec3(0,1,0));
	}
}

void drawBubble(gameConfig *gConfig, sphereProgramNfo *sProgramNfo, mat4 VP, bubble *b)
{
	// mat4 M = scale(mat4(1.0f), vec3(gConfig->boxSize/2.0f));
	float growthPercent = (b->position.y + gConfig->boxSize/2.0f)/gConfig->boxSize;
	if(growthPercent<0.0f) growthPercent = 0.0f;

	mat4 M = translate(b->position) *
			scale(vec3( b->radius *(1.0f + b->growthRatio*(growthPercent))));
	mat4 MVP = VP * M;
	mat4 NM = inverse(transpose(translate(b->position)));

	glUniformMatrix4fv(sProgramNfo->MVPMatrixLocation, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(sProgramNfo->normalMatrixLocation, 1, GL_FALSE, &NM[0][0]);
	glUniformMatrix4fv(sProgramNfo->MMatrixLocation, 1, GL_FALSE, &M[0][0]);

	glUniform4fv(sProgramNfo->colorLocation, 1, &(b->bubbleColor[0]));

	if(b->isSpecial)
		glUniform1i(sProgramNfo->drawIlluminatingLocaiton, 1);
	else
		glUniform1i(sProgramNfo->drawIlluminatingLocaiton, 0);

	glBindBuffer(GL_ARRAY_BUFFER, sphereBufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,	GL_FALSE, 0, (void*)(modelIndex[BOX]*sizeof(GLfloat)) );

	glDrawArrays(GL_TRIANGLES, 0, sphereBufferSize/3);
}

void drawBox( gameConfig *gConfig, modelProgramNfo *mProgramNfo, mat4 VP, bool drawWater)
{
	mat4 M;
	if(!drawWater)
		M = scale(vec3(gConfig->boxSize/2.0f*1.01));
	else
		M = scale(vec3(gConfig->boxSize/2.0f));
	// mat4 M = mat4(1.0f);
	mat4 MVP = VP * M;

	mat4 NM = inverse(transpose(M));

	if(!drawWater)
	{
		glUniform4fv(mProgramNfo->colorLocation, 1, &boxColor[0]);
		glUniform1i(mProgramNfo->drawWaterLocation, 0);
	}
	else
	{
		glUniform4fv(mProgramNfo->colorLocation, 1, &waterColor[0]);
		glUniform1i(mProgramNfo->drawWaterLocation, 1);
	}

	glUniformMatrix4fv(mProgramNfo->MVPMatrixLocation, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(mProgramNfo->normalMatrixLocation, 1, GL_FALSE, &NM[0][0]);
	// glUniformMatrix4fv(mProgramNfo->MVMatrixLocation, 1, GL_FALSE, &MV[0][0]);
	glUniformMatrix4fv(mProgramNfo->MMatrixLocation, 1, GL_FALSE, &M[0][0]);
	

	glBindBuffer(GL_ARRAY_BUFFER, modelBufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,	GL_FALSE, 0, (void*)(modelIndex[BOX]*sizeof(GLfloat)) );

	glBindBuffer(GL_ARRAY_BUFFER, modelNormalBufferID);
	glVertexAttribPointer(1,3,GL_FLOAT, GL_FALSE, 0, (void*)(modelIndex[BOX]*sizeof(GLfloat)) );

	glDrawArrays(GL_TRIANGLES, 0, modelSize[BOX]/3);
}

void drawPortal( gameConfig *gConfig, gameState *gState, modelProgramNfo *mProgramNfo, mat4 VP)
{
	mat4 M;
	M = translate( gState->portalLocation )*scale(gState->portalScale);
	// mat4 M = mat4(1.0f);
	mat4 MVP = VP * M;

	mat4 NM = inverse(transpose(M));

	glUniform4fv(mProgramNfo->colorLocation, 1, &portalColor[0]);
	glUniform1i(mProgramNfo->drawWaterLocation, 0);

	glUniformMatrix4fv(mProgramNfo->MVPMatrixLocation, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(mProgramNfo->normalMatrixLocation, 1, GL_FALSE, &NM[0][0]);
	glUniformMatrix4fv(mProgramNfo->MMatrixLocation, 1, GL_FALSE, &M[0][0]);
	

	glBindBuffer(GL_ARRAY_BUFFER, modelBufferID);
	glVertexAttribPointer(0,3,GL_FLOAT,	GL_FALSE, 0, (void*)(modelIndex[BOX]*sizeof(GLfloat)) );

	glBindBuffer(GL_ARRAY_BUFFER, modelNormalBufferID);
	glVertexAttribPointer(1,3,GL_FLOAT, GL_FALSE, 0, (void*)(modelIndex[BOX]*sizeof(GLfloat)) );

	glCullFace(GL_FRONT);
	glDrawArrays(GL_TRIANGLES, 0, modelSize[BOX]/3);
	glCullFace(GL_BACK);

}

void render(gameConfig * gConfig, gameState *gState,
		sphereProgramNfo *sProgramNfo,modelProgramNfo *mProgramNfo,
		text_renderer *textRend)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 viewMatrix, projMatrix, viewProjectionMatrix;
	viewProjectionMatrices(gState, gConfig, viewMatrix, projMatrix);
	viewProjectionMatrix = projMatrix * viewMatrix;

	glUseProgram(mProgramNfo->modelProgramID);

	// cout<<mProgramNfo->cameraModeLocation<<"\n";
	glUniform1i(mProgramNfo->cameraModeLocation, gState->cameraMode);

	glUniform3fv(mProgramNfo->camPosLocation, 1, &(gState->camPosition[0]));
	glBufferSubData( GL_UNIFORM_BUFFER, 0, sizeof(lights), lights);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	drawBox(gConfig, mProgramNfo, viewProjectionMatrix, 1);
	drawPortal(gConfig, gState, mProgramNfo, viewProjectionMatrix);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	//draw spheres
	glUseProgram(sProgramNfo->sphereProgramID);
	glUniform3fv(sProgramNfo->camPosLocation, 1, &(gState->camPosition[0]));
	glUniform1f(sProgramNfo->boxLimitLocation, gConfig->boxSize/2.0f);

	// cout<<sizeof(light)<<" "<<sizeof(lights)<<"\n";
	glBufferSubData( GL_UNIFORM_BUFFER, 0, sizeof(lights), lights);

	glEnableVertexAttribArray(0);

	if(gState->cameraMode == 1)
		drawBubble(gConfig, sProgramNfo, viewProjectionMatrix, &(gState->playerBubble) );

	bubbleCmp bcmp(gState->camPosition);
	gState->bubbles.sort(bcmp);
	list<bubble>::iterator it;

	for(it = gState->bubbles.begin(); it != gState->bubbles.end(); it++)
		drawBubble(gConfig, sProgramNfo, viewProjectionMatrix, &(*it));

	// drawBubble(gConfig, sProgramNfo, viewProjectionMatrix, b);

	glDisableVertexAttribArray(0);

	// glDisable(GL_DEPTH_TEST);

	if(gState->state == 1)
	{
		string ptsString = "Score: " + intToString(gState->points);
		string lvlString = "Level: " + intToString(gState->currentLevel);
		
		textRend->draw(ptsString, 0.45f, 0.85f, 0.07f, 0.7f);
		textRend->draw(lvlString, 0.45f, 0.75f, 0.07f, 0.7f);
	}

	if(gState->state == 2)
	{
		string loseString = "You are dead!";
		string ptsString = "Your score: " + intToString(gState->points);
		string restartString = "Press enter to restart...";


		textRend->draw(loseString, -0.35f, 0.58f, 0.1f, 0.7f);
		textRend->draw(ptsString, -0.35f, 0.45f, 0.1f, 0.7f);
		textRend->draw(restartString, -0.60f, 0.32f, 0.08f, 0.7f);

	}

	glfwSwapBuffers(window);
}

void spawnBubble( gameState *gState, gameConfig *gConfig, bool random)
{

	if(gState->bubbles.size() < gConfig->defaultMaxBubbles*(1.0f + gState->currentLevel*0.5f))
	{		
		float r = randomPS(1.0f);
		float normalPpb = float(gConfig->defautSpawnPpb*(1.0f + gState->currentLevel*0.2f) );
		float specialPpb = float(gConfig->specialSpawnPpb*(1.0f + gState->currentLevel*0.2f) );

		if(!random || r <= normalPpb )
		{
			// cout<<randomPS(1.0f)<<" "<< gConfig->defautSpawnPpb<<"\n";
			bubble b;
			if(gState->specialBubbleCount < gConfig->specialBubbleLimit && randomPS(1.0f) <= specialPpb)
			{
				b.radius = gConfig->minBubbleSize;
				b.isSpecial = true;
				b.bubbleColor = vec4(specialBubbleColor, 0.9f);
				gState->specialBubbleCount++;

				for(int i=2; i<maxLightCount; i++)
				{
					if(lights[i].isEnabled == false)
					{
						lights[i].isEnabled = true;
						b.lightIndex = i;
						break;
					}
				}

			}
			else
			{
				b.radius = gConfig->minBubbleSize +
							randomPS(gConfig->maxBubbleSize- gConfig->minBubbleSize)*(gState->currentLevel*0.05f + 1.0f);
				b.isSpecial = false;
				b.bubbleColor = vec4( bubbleColors[rand() % 7], 0.8f);
			}
			// cout<<b.radius<<"\n";
			b.speed = gConfig->minBubbleSpeed +
						(randomPS(gConfig->maxBubbleSpeed-gConfig->minBubbleSpeed)) *
						(gState->currentLevel * 0.2f + 1.0f);

			b.growthRatio = gConfig->bubbleGrowthRatio;
			float d = gConfig->boxSize/2.0f - 1.2f * b.radius;

			float posX = randomPS(2.0f * d)-d;
			float posZ = randomPS(2.0f * d)-d;

			b.position = vec3(posX, -(gConfig->boxSize/2.0f + 1.5f *b.radius  ), posZ);
			// cout<<posX<<" "<<posZ<<"\n";
			gState->bubbles.push_back(b);
		}
	}
}

void update(double deltaTime, gameConfig *gConfig, gameState *gState)
{
	// vec3 diff = vec3(0.0f, 0.0f, 0.0f);
	// cout<<gState->playerPosition.z<<"\n";

	float zoomDiff = mouseScroll/24.0f;
	if(gState->cameraMode == 0)
	{
		gState->FPPzoom -= zoomDiff;
		if(gState->FPPzoom < 0.3f) gState->FPPzoom = 0.3f;
		if(gState->FPPzoom > 1.2f) gState->FPPzoom = 1.2f;
		// cout<<gState->FPPzoom<<"\n";
	}
	else
	{
		gState->OUTzoom -= zoomDiff;
		if(gState->OUTzoom < 0.3f) gState->OUTzoom = 0.3f;
		if(gState->OUTzoom > 1.2f) gState->OUTzoom = 1.2f;

		gState->OUTyaw -= mouseMoveX*0.05*deltaTime;
		if(gState->OUTyaw > 1.0f ) gState->OUTyaw = 1.0f;
		if(gState->OUTyaw < -1.0f ) gState->OUTyaw = -1.0f;

		gState->OUTpitch -= mouseMoveY*0.05*deltaTime;
		if(gState->OUTpitch > 1.0f ) gState->OUTpitch = 1.0f;
		if(gState->OUTpitch < -1.0f ) gState->OUTpitch = -1.0f;
	}

	mouseScroll = 0.0;

	if(gState->state == 1) //playing
	{
		spawnBubble( gState, gConfig, 1 );

		vec3 dec = vec3(0.0f), acc=vec3(0.0f);
		if(length(gState->playerSpeed) > 0.0f )
			dec = -vec3(deltaTime*gConfig->playerDeceleration)*normalize(gState->playerSpeed);

		if( pressedKeys[GLFW_KEY_SPACE] || pressedKeys[GLFW_KEY_PAGE_UP])
			acc += vec3(0.0f, 1.0f, 0.0f);

		if( pressedKeys[GLFW_KEY_LEFT_SHIFT] || pressedKeys[GLFW_KEY_PAGE_DOWN])
				acc -= vec3(0.0f, 1.0f, 0.0f);

		if(!gState->cameraMode) //fpp view
		{

			gState->playerDirection = rotateY(gState->playerDirection, float(-deltaTime*mouseMoveX*2.0) );
			// cout<<gState->playerDirection.x<<" "<<gState->playerDirection.z<<"\n";
			// cout<<mouseMoveX<<"\n";
			gState->FPPpitch -= mouseMoveY*0.05*deltaTime;
			if(gState->FPPpitch > 1.0f ) gState->FPPpitch = 1.0f;
			if(gState->FPPpitch < -1.0f ) gState->FPPpitch = -1.0f;


			vec3 normalizedDir = normalize(gState->playerDirection);

			if(pressedKeys[GLFW_KEY_W] || pressedKeys[GLFW_KEY_UP])
				acc += normalizedDir;

			if(pressedKeys[GLFW_KEY_S] || pressedKeys[GLFW_KEY_DOWN])
				acc -= normalizedDir;

			if(pressedKeys[GLFW_KEY_A] || pressedKeys[GLFW_KEY_LEFT])
				acc -= vec3(-normalizedDir.z, 0.0f, normalizedDir.x);

			if(pressedKeys[GLFW_KEY_D] || pressedKeys[GLFW_KEY_RIGHT])
				acc += vec3(-normalizedDir.z, 0.0f, normalizedDir.x);;


			if(length(acc) > 0.0f)
				acc = normalize(acc)*vec3(deltaTime*gConfig->playerAcceleration);

			gState->playerSpeed += (acc + dec);

			float d = length(gState->playerSpeed);
			if( d > 0.0f)
			{
				if(d > gConfig->playerMaxSpeed)
				{
					d = gConfig->playerMaxSpeed;
					gState->playerSpeed = vec3(d)*normalize(gState->playerSpeed);
				}
			}
			// cout<<gState->playerSpeed.x<<" "<<gState->playerSpeed.y<<" "<<gState->playerSpeed.z<<"\n";

		}
		else
		{

			if(pressedKeys[GLFW_KEY_W] || pressedKeys[GLFW_KEY_UP])
				acc += vec3(0.0f, 0.0f, 1.0f);

			if(pressedKeys[GLFW_KEY_S] || pressedKeys[GLFW_KEY_DOWN])
				acc -= vec3(0.0f, 0.0f, 1.0f);

			if(pressedKeys[GLFW_KEY_A] || pressedKeys[GLFW_KEY_LEFT])
				acc += vec3(1.0f, 0.0f, 0.0f);

			if(pressedKeys[GLFW_KEY_D] || pressedKeys[GLFW_KEY_RIGHT])
				acc -= vec3(1.0f, 0.0f, 0.0f);

			if(length(acc) > 0.0f)
				acc = normalize(acc)*vec3(deltaTime*gConfig->playerAcceleration);

			gState->playerSpeed += (acc + dec);

			float d = length(gState->playerSpeed);
			if( d > 0.0f)
			{
				if(d > gConfig->playerMaxSpeed)
				{
					d = gConfig->playerMaxSpeed;
					gState->playerSpeed = vec3(d)*normalize(gState->playerSpeed);
				}
			}
		}
	
		float bound = gConfig->boxSize/2.0f - gConfig->playerSize/1.5f;
		vec3 newPos = gState->playerPosition + vec3(deltaTime) * gState->playerSpeed;

		if(newPos.x > bound || newPos.x < -bound)
			gState->playerSpeed.x = 0;

		if(newPos.y > bound || newPos.y < -bound)
			gState->playerSpeed.y = 0;

		if(newPos.z > bound || newPos.z < -bound)
			gState->playerSpeed.z = 0;

		gState->playerPosition += vec3(deltaTime) * gState->playerSpeed;


		if(!gState->cameraMode)
		{
			gState->camPosition = gState->playerPosition;
			gState->camDirection = gState->playerDirection;
		}
		else
		{
			gState->camPosition = vec3( gConfig->boxSize/1.85f, 0.0f, -gConfig->boxSize/1.85f );
			gState->playerBubble.position = gState->playerPosition;
		}

		list<bubble>::iterator it;
		list<bubble>::iterator it2;
		for(it = gState->bubbles.begin(); it != gState->bubbles.end(); )
		{
			bubble *b = &(*it);
			b->position += vec3(0.0f, b->speed*deltaTime, 0.0f);

			vec3 d = b->position - gState->playerPosition;
			if(length(d) < b->radius + gState->playerBubble.radius)
			{
				if(b->isSpecial)
				{
					gState->points += gState->currentLevel*5;
					lights[b->lightIndex].isEnabled = false;

					it2 = it;
					it++;
					gState->bubbles.erase(it2);
				}
				else
				{
					gState->state = 2;
				}

			}

			if(b->isSpecial)
				lights[b->lightIndex].position = b->position;
			
			if(b->position.y >= gConfig->boxSize/2.0f +
					1.5f*(b->radius  + b->radius*b->growthRatio))
			{
				if(b->isSpecial)
					lights[b->lightIndex].isEnabled = false;

				it2 = it;
				it++;
				gState->bubbles.erase(it2);
			}
			else
				it++;
		}
		lights[1].position = gState->playerPosition;
		// cout<<gState->portalLocation.z - gState->playerPosition.z<<"\n";
		if(gState->portalLocation.z - gState->playerPosition.z <= gConfig->playerSize)
		{
			if( gState->playerPosition.x <= gState->portalLocation.x + gState->portalScale.x &&
				gState->playerPosition.x >= gState->portalLocation.x - gState->portalScale.x )
				if( gState->playerPosition.y <= gState->portalLocation.y + gState->portalScale.y &&
				gState->playerPosition.y >= gState->portalLocation.y - gState->portalScale.y )
				{
					nextLevel(*gState, *gConfig);
				}
		}
	}

	if(gState->state == 2)
	{
		
		
		if(!gState->cameraMode)
		{
			gState->camPosition = gState->playerPosition;
			gState->camDirection = gState->playerDirection;
		}
		else
		{
			gState->camPosition = vec3( gConfig->boxSize/1.85f, 0.0f, -gConfig->boxSize/1.85f );
			gState->playerBubble.position = gState->playerPosition;
		}

		// if(pressedKeys[GLFW_KEY_SPACE)
		// {
		// 	reset(*gState, *gConfig);
		// }
	}
}

void initLights()
{

	//global light
	lights[0].position = vec3(1.0f, 1.0f, -1.0f);
	lights[0].ambient = vec3(0.15f, 0.15f, 0.15f);
	lights[0].diffuse = vec3(0.2f, 0.2f, 0.2f);
	lights[0].specular = vec3(0.15f, 0.15f, 0.15f);

	lights[0].lightType = 0;
	lights[0].isEnabled = 1;
	lights[0].isDistanceLight = 0;
	lights[0].specularPower = 20;

	//player light
	lights[1].position = vec3(1.0f, 1.0f, 0.0f);
	lights[1].ambient = playerColor*vec3(0.05f, 0.05f, 0.05f);
	lights[1].diffuse = vec3(0.5)*playerColor;
	lights[1].specular = vec3(0.15f,0.15f,0.10f);

	lights[1].lightType = 1;
	lights[1].isEnabled = 1;
	lights[1].isDistanceLight = 1;
	lights[1].specularPower = 20;

	for(int i=2; i<maxLightCount; i++)
	{
		lights[i].position = vec3(1.0f, 1.0f, 0.0f);
		lights[i].ambient = specialBubbleColor*vec3(0.03f);
		lights[i].diffuse = vec3(0.3)*specialBubbleColor;
		lights[i].specular = vec3(0.2)*specialBubbleColor;;

		lights[i].lightType = 1;
		lights[i].isEnabled = 0;
		lights[i].isDistanceLight = 1;
		lights[i].specularPower = 25;
	}
	// lights[1].position = vec4(gState->playerPosition, 1.0f);
	// lights[1].isEnabled = 1;
}

void initBuffers()
{
	// cout<<sizeof(float)<<" "<<sizeof(int)<<" "<<sizeof(vec4)<<"\n";
	// mat4 rotY = rotate(M_PintI_2/double(sphereResX), vec3(0.0, 0.0, 1.0) );
	for(int h=0; h<2; h++)
	{
		for(int i=0; i<sphereResY-1; ++i)
		{
			vec3 v[4];
			// cout<< float(180.0/double(sphereResX))<<"\n";
			v[0] = rotateZ(vec3(sphereRadius, 0.0, 0.0), float((1-2*(h==1))*90.0/float(sphereResY) * i) );
			v[1] = rotateZ(vec3(sphereRadius, 0.0, 0.0), float((1-2*(h==1))*90.0/double(sphereResY) * (i+1)) );
			v[2] = rotateY(v[1], float((1-2*(h==1))*-360.0/double(sphereResX)));
			v[3] = rotateY(v[0], float((1-2*(h==1))*-360.0/double(sphereResX)));		

			for(int j=0; j<sphereResX; ++j)
			{
				for(int k=0; k<2; k++)
					for(int l=0; l<3; l++)
					{
						sphereBuffer[sphereBufferSize++] = v[ (k*2 + l) % 4].x;
						sphereBuffer[sphereBufferSize++] = v[ (k*2 + l) % 4].y;
						sphereBuffer[sphereBufferSize++] = v[ (k*2 + l) % 4].z;
					}

				for(int k=0; k<4; k++)
					v[k] = rotateY(v[k], float(360.0/double(sphereResX)) );
			}
		}

		vec3 v[2];
		v[0] = rotateZ(vec3(sphereRadius, 0.0, 0.0), float((1-2*(h==1))*90.0/float(sphereResY) * (sphereResY-1) ));
		v[1] = rotateY(v[0], float((1-2*(h==1))*360.0/double(sphereResX)) );

		for(int j=0; j<sphereResX; ++j)
		{

			for(int k=0;k<2; k++)
			{
				sphereBuffer[sphereBufferSize++] = v[k].x;
				sphereBuffer[sphereBufferSize++] = v[k].y;
				sphereBuffer[sphereBufferSize++] = v[k].z;
			}

			sphereBuffer[sphereBufferSize++] = 0.0f;
			sphereBuffer[sphereBufferSize++] = (1-2*(h==1))*sphereRadius;
			sphereBuffer[sphereBufferSize++] = 0.0f;

			for(int k=0; k<2; k++)
				v[k] = rotateY(v[k], float(360.0/double(sphereResX)) );
		}
	}

	vec3 v[4] = { vec3(-1.0f, 0.0f, -1.0f), vec3(1.0f, 0.0f, -1.0f),
				  vec3(1.0f, 0.0f, 1.0f), vec3(-1.0f, 0.0f, 1.0f)};

	vec3 axes[3] = { vec3(1.0, 0.0, 0.0), vec3(0.0, 0.0, 1.0), vec3(1.0, 0.0, 0.0) };
	float angles[3][2] = { {180.0f, 0.0f}, {90.0f, -90.0f}, {-90.0f, 90.0f}  };
	vec3 normals[3] = { vec3(0.0, 1.0, 0.0), 
						vec3(1.0, 0.0, 0.0),
						vec3(0.0, 0.0, 1.0)};
	
	modelIndex[BOX] = 0;
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<2; j++)
		{
			// vec3 vn = rotate(vec3(0.0f, -1.0f, 0.0f), angles[i][j], axes[i]);
			int sgn = -((j==1)*(-2) + 1);
			vec3 vn = normals[i*2 + j];
			for(int k=0; k<2; k++)
				for(int l=0; l<3; l++)
				{
					vec3 vp = rotate(v[ (k*2 + l) % 4], angles[i][j], axes[i]);
					modelBuffer[modelBufferSize++] = vp.x + sgn*normals[i].x;
					modelBuffer[modelBufferSize++] = vp.y + sgn*normals[i].y;
					modelBuffer[modelBufferSize++] = vp.z + sgn*normals[i].z;
				}

			for(int k=0; k<6; k++)
			{
				modelNormalBuffer[modelNormalBufferSize++] = -sgn*normals[i].x;
				modelNormalBuffer[modelNormalBufferSize++] = -sgn*normals[i].y;
				modelNormalBuffer[modelNormalBufferSize++] = -sgn*normals[i].z;
			}
		}
	}
	modelSize[BOX] = modelBufferSize;

	glGenBuffers(1, &sphereBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, sphereBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * sphereBufferSize, sphereBuffer, GL_STATIC_DRAW);

	glGenBuffers(1, &modelBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, modelBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * modelBufferSize, modelBuffer, GL_STATIC_DRAW);

	glGenBuffers(1, &modelNormalBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, modelNormalBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * modelNormalBufferSize, modelNormalBuffer, GL_STATIC_DRAW);	

	glGenBuffers(1, &lightsUniformBufferID);
	glBindBuffer(GL_UNIFORM_BUFFER, lightsUniformBufferID);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(lights), &lights, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, lightsBindingPoint, lightsUniformBufferID);
}

void scrollCallbackFunc(GLFWwindow * window, double dX, double dY)
{
	mouseScroll += dY;
}


void handleInput(bool &doExit, gameState *gState, gameConfig *gConfig)
{
	glfwPollEvents();

	if(glfwGetKey(window, GLFW_KEY_ESCAPE ) == GLFW_PRESS)
		doExit = true;

	static uint keysToCheck[] =
		{ GLFW_KEY_LEFT, GLFW_KEY_RIGHT, GLFW_KEY_UP, GLFW_KEY_DOWN,
		  GLFW_KEY_PAGE_UP, GLFW_KEY_PAGE_DOWN,
		  GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_A, GLFW_KEY_D, 
		  GLFW_KEY_SPACE, GLFW_KEY_LEFT_SHIFT
		};

	for(int i=0; i<12; i++)
	{
		if(glfwGetKey(window, keysToCheck[i] ) == GLFW_PRESS)
			pressedKeys[keysToCheck[i]] = true;

		if(glfwGetKey(window, keysToCheck[i] ) == GLFW_RELEASE)
			pressedKeys[keysToCheck[i]] = false;
	}

	static bool tabPressed = false;
	if(glfwGetKey(window, GLFW_KEY_TAB ) == GLFW_RELEASE) tabPressed = false;
	if(glfwGetKey(window, GLFW_KEY_TAB ) == GLFW_PRESS && !tabPressed)
	{
		tabPressed = true;

		if(gState->cameraMode == 0)
			gState->cameraMode = 1;
		else
			gState->cameraMode = 0;
	}

	static bool enterPressed = false;
	if(glfwGetKey(window, GLFW_KEY_ENTER ) == GLFW_RELEASE) enterPressed = false;
	if(glfwGetKey(window, GLFW_KEY_ENTER ) == GLFW_PRESS && !enterPressed)
	{
		enterPressed = true;

		if(gState->state == 2)
			reset(*gState, *gConfig);
	}

	double mX, mY;
	glfwGetCursorPos(window, &mX, &mY);

	mouseMoveX = mX - resX/2.0;
	mouseMoveY = mY - resY/2.0;
	glfwSetCursorPos(window, resX/2.0, resY/2.0);
}

int main( void )
{
	srand(time(NULL));

	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow( resX, resY, "Bubbles", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glewExperimental = true;
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}


	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	glfwSetCursorPos(window, resX/2.0, resY/2.0);
	glfwSetScrollCallback(window, scrollCallbackFunc);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	sphereProgramNfo sProgramNfo = loadSphereProgram();
	modelProgramNfo mProgramNfo = loadModelProgram();

	// gameConfig(float boxSize, float playerSize, float playerAcceleration, float playerDeceleration,
	// 			float playerMaxSpeed, float minimumBubbleSize, float maximumBubbleSize,
	// 			float bubbleGrowthRatio, float bubbleGrowthPerLevel)
	gameConfig gConfig;
	gameState gState;
	reset(gState, gConfig);



	glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, 0.0f);


	// GLuint programID = LoadShaders( "bubbles_model_vs.vertexshader", "bubbles_model_fs.fragmentshader" );

	// GLuint programID = LoadShaders( "SimpleTransform.vertexshader", "SingleColor.fragmentshader" );
	// GLuint MatrixID = glGetUniformLocation(programID, "MVP");

	initBuffers();
	initLights();
	text_renderer textRend;

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	bool doExit = false;
	double currentTime = glfwGetTime();
	double acc = 0.0f;

	do
	{	
		acc += glfwGetTime() - currentTime;
		currentTime = glfwGetTime();

		while(acc >= deltaTime)
		{
			handleInput(doExit, &gState, &gConfig);
			update(deltaTime, &gConfig, &gState);
			acc -= deltaTime;
		}

		render(&gConfig, &gState, &sProgramNfo, &mProgramNfo, &textRend);
	}
	while( !doExit && glfwWindowShouldClose(window) == 0);
	glfwTerminate();

	glDeleteVertexArrays(1, &VertexArrayID);

	return 0;
}