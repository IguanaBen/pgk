#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vertexUV;

out vec2 UV;


void main(){

	UV = vertexUV;
	gl_Position = vec4(position.x, position.y, -0.9999f, 1.0f);
	//gl_Position = vec4(position,1);
	
}
