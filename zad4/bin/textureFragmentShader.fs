#version 330

out vec4 color;
in vec2 UV;
uniform sampler2D textSampler;

void main()
{	
	vec4 col = texture2D( textSampler, UV );

	if(col.x < 0.07 && col.y < 0.07 && col.z < 0.07)
		discard;

	color = texture2D( textSampler, UV );
}