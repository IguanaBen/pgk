#include "text_renderer.h"


text_renderer::text_renderer()
{
	fontProgram = LoadShaders("textureVertexShader.vs", "textureFragmentShader.fs");
	font_texture = loadBMP_custom("font1.bmp");
	texture_id  = glGetUniformLocation(fontProgram, "textSampler");
	vs_position = glGetAttribLocation(fontProgram, "position");
	vertexUVID = glGetAttribLocation(fontProgram, "vertexUV");

	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);

	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
}


void text_renderer::draw(std::string text, GLfloat x, GLfloat y, GLfloat size, GLfloat span)
{
	std::vector<glm::vec2> vertices;
	std::vector<glm::vec2> UVs;

	const int offset = -32;
	// GLfloat letter_height = 16.0;

	for ( unsigned int i=0 ; i<text.length() ; i++ ){

		glm::vec2 vertex_up_left    = glm::vec2( x+span*i*size     , y+size );
		glm::vec2 vertex_up_right   = glm::vec2( x+span*i*size+size, y+size );
		glm::vec2 vertex_down_right = glm::vec2( x+span*i*size+size, y      );
		glm::vec2 vertex_down_left  = glm::vec2( x+span*i*size     , y      );

		vertices.push_back(vertex_up_left);
		vertices.push_back(vertex_down_left);
		vertices.push_back(vertex_up_right);

		vertices.push_back(vertex_down_right);
		vertices.push_back(vertex_up_right);
		vertices.push_back(vertex_down_left);

		char character = text[i] + offset;
		float uv_x = (character%16)/16.0f;
		float uv_y = (character/16)/16.0f;

		glm::vec2 uv_up_left    = glm::vec2( uv_x           , -uv_y );
		glm::vec2 uv_up_right   = glm::vec2( uv_x+1.0f/16.0f, -uv_y );
		glm::vec2 uv_down_right = glm::vec2( uv_x+1.0f/16.0f, -(uv_y + 1.0f/16.0f) );
		glm::vec2 uv_down_left  = glm::vec2( uv_x           , -(uv_y + 1.0f/16.0f) );
		UVs.push_back(uv_up_left   );
		UVs.push_back(uv_down_left );
		UVs.push_back(uv_up_right  );

		UVs.push_back(uv_down_right);
		UVs.push_back(uv_up_right);
		UVs.push_back(uv_down_left);
	}

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), &vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, UVs.size() * sizeof(glm::vec2), &UVs[0], GL_STATIC_DRAW);

	glUseProgram(fontProgram);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, font_texture);

	glUniform1i(texture_id, 0);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(vs_position, 2, GL_FLOAT, GL_FALSE, 0, (void*)0 );

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(vertexUVID, 2, GL_FLOAT, GL_FALSE, 0, (void*)0 );

	// glEnable(GL_BLEND);
	// glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDrawArrays(GL_TRIANGLES, 0, vertices.size() );

	// glDisable(GL_BLEND);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}