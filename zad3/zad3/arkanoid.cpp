/*
Michał Gańczorz
*/

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <time.h>

#define GLEW_STATIC

#include <GL/glew.h>

#include <GLFW/glfw3.h>
GLFWwindow* window;

#include <glm/glm.hpp>
using namespace glm;
using namespace std;

#include <common/shader.hpp>

const uint resX = 900;
const uint resY = 700;

const uint paddleWidth = 180;
const uint paddleHeight = 36;

const double paddleStartY = 130;

const uint ballRadius = 26;
const double ballOutline = 0.83;
const uint ballSteps = 20;

const uint cornerRadius = 10;
const uint blockHeight = 26;
const uint blockWidth = 60;

const double backgroundLineWidth = 3.5;
const uint hexRadius = 80;

const uint boardHexRadius = 380;
const double deltaTime = 0.01666;

const uint spacing = 0;
const uint startingHeight = 470;
const uint numberBlocks[] = {12, 10, 10, 10, 10};
uint totalBlocks = 0;
uint points = 0;
string pointsStr;

uint currentGameState = 0;

// vec3 cardFrontColor = vec3(1.0, 1.0, 1.0);
// vec3 cardBackColor1 = vec3(0.0, 51.0/255.0, 102.0/255.0);
// vec3 cardBackColor2 = vec3(51.0/255.0, 102.0/255.0, 153.0/255.0);
// vec3 selectionColor = vec3(1.0, 1.0, 0.0);
// vec3 backgroundColor = vec3(102.0/255.0,102.0/255.0,102.0/255.0);
// vec3 boardLineColor = vec3(204.0/255.0,153.0/255.0,51.0/255.0);

// vec3 hudColor = vec3(100.0/255.0, 255.0/255.0, 0.0/255.0);

int gameState[20][5];

vec3 paddleColor = vec3(108.0/255.0, 123.0/255.0, 139.0/255.0);
vec3 blockOutlineColor = vec3(0.0, 0.0, 0.0);
vec3 backgroundColor = vec3(0.1f, 0.1f, 0.5f);
vec3 boardColor = vec3(0.1f, 0.1f, 0.85f);
vec3 backgroundLineColor = vec3(1.0/255.0, 1.0/255.0, 140.0/255.0);
vec3 boardBoardersColor = vec3(1.0/255.0, 1.0/255.0, 100.0/255.0);
vec3 ballColor = vec3(207.0/255.0, 10.0/255.0, 42.0/255.0);


const vec3 blockColors[] =
{
	vec3(1.0, 190.0/255.0, 0.0),
	vec3(110.0/255.0, 230.0/255.0, 42.0/255.0),
	vec3(172.0/255.0, 84.0/255.0, 170.0/255.0),
	vec3(251.0/255.0, 115.0/255.0, 116.0/255.0),
	vec3(197.0/255.0, 170.0/255.0, 245.0/255.0),
	vec3(226.0/255.0, 61.0/255.0, 128.0/255.0),
	vec3(19.0/255.0, 161.0/255.0, 203.0/255.0),
	vec3(153.0/255.0, 255.0/255.0, 0.0/255.0),
	vec3(157.0/255.0, 190.0/255.0, 42.0/255.0),
	vec3(172.0/255.0, 84.0/255.0, 170.0/255.0)
};

#define BLOCK0 0
#define BLOCK1 1
#define BLOCK2 2
#define BLOCK3 3
#define BLOCK4 4

#define HEXLINE 5
#define BOARDBORDERS 6
#define BALL 7
#define PADDLE 8
#define BACKGROUND 9



const uint maxBufferVertsCount = 10000;
GLfloat shapeBuffer[maxBufferVertsCount * 2];
GLfloat colorBuffer[maxBufferVertsCount * 3];

uint shapeBufferSize = 0;
uint colorBufferSize = 0;

uint shapeIndexStart[100];
uint shapeSize[100];

uint colorIndexStart[100];
uint colorSize[100];

vec2 boardHexCoords[6];

bool pressedKeys[456];


GLuint programID;
GLuint shapeBufferID;
GLuint colorBufferID;
// GLuint colorLocation;
GLuint transformLocation;
GLuint scaleLocation;
GLuint hexCoordLocation;
GLuint gameStateLocation;

void drawShape(uint shape, vec2 transform)
{
	glUniform2fv(transformLocation, 1, &transform[0]);
	glUniform2f(scaleLocation, 1.0, 1.0);

	switch(shape)
	{
		case BLOCK0:
		case BLOCK1:
		case BLOCK2:
		case BLOCK3:
		case BLOCK4:
		case BOARDBORDERS:
		case BALL:
		case PADDLE:
		case BACKGROUND:
			glBindBuffer(GL_ARRAY_BUFFER, shapeBufferID);
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)(shapeIndexStart[shape]*sizeof(GLfloat)));
			
			glBindBuffer(GL_ARRAY_BUFFER, colorBufferID);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0,  (void*)(colorIndexStart[shape]*sizeof(GLfloat)));

			glDrawArrays(GL_TRIANGLES, 0, shapeSize[shape]/2);
		break;

		case HEXLINE:
			glBindBuffer(GL_ARRAY_BUFFER, shapeBufferID);
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)(shapeIndexStart[shape]*sizeof(GLfloat)));
			
			glBindBuffer(GL_ARRAY_BUFFER, colorBufferID);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0,  (void*)(colorIndexStart[shape]*sizeof(GLfloat)));

			glDrawArrays(GL_LINE_STRIP, 0, shapeSize[HEXLINE]/2);
		break;
	}
}

double paddlePositionX = 0;
double paddlePositionY = 0;

double paddleSpeed = 250.0;

double ballPosX = 0;
double ballPosY = 0;

vec2 ballDirection = vec2(0.0, 1.0);
double ballSpeed = 350;

double angle( vec2 v1, vec2 v2)
{
	return acos( dot(normalize(v1),normalize(v2)));
}

vec2 rotateZ(vec2 p, double rads)
{	
	return vec2(p.x*cos(rads) - p.y*sin(rads), p.x*sin(rads) + p.y*cos(rads) );
}

double randomPS(double maxval)
{
	return maxval*(double(rand() % 10000)/10000.0);
}

bool inRectPt(vec2 lowerLeftCorner, vec2 upperRightCorner, vec2 pt)
{
	return (pt.x >= lowerLeftCorner.x) && (pt.x <= upperRightCorner.x)
		  && (pt.y >= lowerLeftCorner.y) && (pt.y <= upperRightCorner.y);
}

bool recCol(vec2 r1c1, vec2 r1c2, vec2 r2c1, vec2 r2c2)
{
	return(inRectPt(r1c1, r1c2, r2c1) || inRectPt(r1c1, r1c2, r2c2) 
			|| inRectPt(r2c1, r1c2, r1c1) || inRectPt(r2c1, r2c2, r1c2) 
			);
}

void reset()
{
	pointsStr = "0";
	totalBlocks = 0;
	points = 0;
	for(int i=0; i<5; i++)
	{
		for(int j=0; j<numberBlocks[i]; j++)
			gameState[j][i] = i;

		totalBlocks += numberBlocks[i];
	}

	paddlePositionX = double(resX)/2.0;
	paddlePositionY = paddleStartY;

	ballPosX = double(resX)/2.0;
	ballPosY = paddleStartY + ballRadius - 4;

	ballDirection = vec2(-0.5 + randomPS(1.0),  -0.5 + randomPS(1.0));
}


void update(double deltaTime)
{
	if(currentGameState != 1)
		return;

	if(pressedKeys[GLFW_KEY_LEFT])
	{
		double boundX = double(resX)/2.0 - paddlePositionY/0.57735027 + paddleSpeed*deltaTime ;

		if(boundX <= paddlePositionX - paddleHeight*0.5 - 5)
			paddlePositionX -= paddleSpeed*deltaTime;
	}

	if(pressedKeys[GLFW_KEY_RIGHT])
	{
		double boundX = double(resX)/2.0 + paddlePositionY/0.57735027 - paddleSpeed*deltaTime ;

		if(boundX >= paddlePositionX + paddleHeight*0.5 + 5)
			paddlePositionX += paddleSpeed*deltaTime;
	}

	vec2 deltaSBall = normalize(ballDirection);

	double newBallPosX = ballPosX + deltaSBall.x*deltaTime*ballSpeed;
	double newBallPosY = ballPosY + deltaSBall.y*deltaTime*ballSpeed;

	for(int i=0; i<6; i++)
	{

		vec2 v1 = boardHexCoords[(i+1) % 6] - boardHexCoords[i];
		vec2 colVec = vec2(v1.y, -v1.x);
		colVec = normalize(colVec);
		vec2 v2 = vec2(newBallPosX + colVec.x*ballRadius*0.5, newBallPosY + colVec.y*ballRadius*0.5)  - boardHexCoords[(i+1) % 6];

		// cout<<int(boardHexCoords[i].x)<<" "<<int(boardHexCoords[i].y)<<"\n";

		if( (v1.x*v2.y) - (v1.y*v2.x) <= 0 )
		{
			// cout<<angle(v1, ballDirection)<<"\n";
			ballDirection = rotateZ(ballDirection, angle(v1, ballDirection)*2.0);
			if(i==3)
			{
				currentGameState = 2;
				glfwSetWindowTitle(window, ("Arkanoid | Game Over | Points: " + pointsStr).c_str() );
				return;
			}
			// cout<<"coll\n";
		}
	}
	// cout<<paddlePositionY<<"\n";
	if(
		( newBallPosY - ballRadius*0.25 <= paddlePositionY + paddleHeight*0.25 )
		&&
		( newBallPosY + ballRadius*0.25 >= paddlePositionY - paddleHeight*0.25 )
	  )
	{
		
		// cout<<newBallPosX<<" "<<paddlePositionX<<"\n";
		if(
			( newBallPosX >= paddlePositionX - paddleWidth*0.25 )
			&&
			( newBallPosX <= paddlePositionX + paddleWidth*0.25 )
		  )	
		{
			// cout<<"kk\n";	
			// ballDirection = rotateZ(ballDirection, M_PI_4 - randomPS(M_PI_2));
			ballDirection = vec2(ballDirection.x , -ballDirection.y);
			// cout<<randomPS(0.4)<<"\n";
			ballDirection = normalize(ballDirection);

			ballDirection = ballDirection + vec2( randomPS(1.0) - 0.5, randomPS(1.0) - 0.5);
			ballDirection = normalize(ballDirection);
		}
		else
		{
			if(
				( newBallPosX + ballRadius*0.25 >= paddlePositionX - paddleWidth*0.25 - paddleHeight*0.25 )
				&&
				( newBallPosX - ballRadius*0.25 <= paddlePositionX + paddleWidth*0.25 + paddleHeight*0.25 )
		 	 )
			{
				// cout<<"aa\n";
				vec2 v1;
				if(newBallPosX <= paddlePositionX - paddleWidth*0.5)
					v1 = vec2(paddlePositionX - paddleWidth*0.5 - newBallPosX, paddlePositionY - newBallPosY);
				else
					v1 = vec2(paddlePositionX + paddleWidth*0.5 - newBallPosX, paddlePositionY - newBallPosY);
				vec2 colVec = vec2(v1.y, -v1.x);
				colVec = normalize(colVec);
				// ballDirection = rotateZ(ballDirection, M_PI_4 - randomPS(M_PI_2));
				ballDirection = rotateZ(ballDirection, angle(colVec, v1)*2.0);
				// ballDirection = ballDirection + vec2( randomPS(1.0) - 0.5, randomPS(1.0) - 0.5);
				ballDirection = normalize(ballDirection);

			}
		}
		// if(
		// 	( newBallPosX + ballRadius*0.5 >= paddlePositionX - paddleWidth*0.5 )
		// 	&&
		// 	( newBallPosX - ballRadius*0.5 <= paddlePositionX + paddleWidth*0.5 )
		//   )
	}

	bool cnt = true;

	vec2 ballCorner1 = vec2( newBallPosX - double(ballRadius)/2.0,  newBallPosY- double(ballRadius)/2.0);
	vec2 ballCorner2 = vec2( newBallPosX + double(ballRadius)/2.0, newBallPosY + double(ballRadius)/2.0 );


	for(int i=0; i<5 && cnt; i++)
	{
		vec2 blockPos =  vec2( double(resX)/2.0 -blockWidth*(numberBlocks[i]/2) +(numberBlocks[i] % 2 == 0)*blockWidth/2.0,
							   double(startingHeight) + i*blockHeight);

		vec2 blockPosC1 = blockPos - vec2(double(blockWidth)/2.0, double(blockHeight)/2.0);
		vec2 blockPosC2 = blockPos + vec2(double(blockWidth)/2.0, double(blockHeight)/2.0);


		for(int j=0; j<numberBlocks[i]; j++)
		{
			// cout<<gameState[j][i]<<"a\n"
			vec2 blockPosC1 = blockPos - vec2(double(blockWidth)/2.0, double(blockHeight)/2.0);
			vec2 blockPosC2 = blockPos + vec2(double(blockWidth)/2.0, double(blockHeight)/2.0);
			// cout<<blockPosC1.x<<" "<<blockPosC1.y<<"\n";
			// cout<<blockPosC2.x<<" "<<blockPosC2.y<<"\n";

			if(gameState[j][i] >=0 && recCol(ballCorner1, ballCorner2, blockPosC1, blockPosC2 ) )
			{	

				vec2 dr = vec2(newBallPosX, newBallPosY) -blockPos;
				vec2 ndr = normalize(dr);

				bool collide = false;

				collide = collide || (length(dr) <= ballRadius/2.0);

				// cout<<dr.x<<" "<<dr.y<<"\n";
				vec2 drp = dr;
				drp = drp - vec2(ndr.x*ballRadius*0.5, ndr.y*ballRadius*0.5);
				collide = collide ||inRectPt(vec2(-double(blockWidth)/2.0, -double(blockHeight)/2.0), 
							   vec2(double(blockWidth)/2.0, double(blockHeight)/2.0),drp);
				// cout<<collide<<"\n";
				// dr = drp;
				if(collide)
				{
					bool hCol = ( -double(blockWidth)/2.0 <= dr.x && dr.x <= double(blockWidth)/2.0);
					bool vCol = ( -double(blockWidth)/2.0 >= dr.x || dr.x >= double(blockWidth)/2.0);
					// cout<<hCol<<" "<<vCol<<" "<<-double(blockHeight)/2.0<<" "<<dr.x<<"\n";
					// ballDirection = vec2(-ballDirection.x,-ballDirection.y);
					// cout<<dr.x<<" "<<vCol<<"\n";
					ballDirection = vec2((hCol)*2.0*ballDirection.x-ballDirection.x, (vCol)*2.0*ballDirection.y-ballDirection.y);

					totalBlocks--;
					points++;

					std::stringstream ss;
					ss<<points;

					pointsStr = ss.str();

					if(totalBlocks == 0)
					{
						glfwSetWindowTitle(window, ("Arkanoid | You Won! | Points: " + pointsStr).c_str() );
						currentGameState = 3;
					}
					else
						glfwSetWindowTitle(window, ("Arkanoid | Playing | Points: " + pointsStr).c_str() );

					gameState[j][i] = -1;
					cnt = false;
					break;
				}
			}

			blockPos += vec2(double(blockWidth), 0.0);
		}
	}


	deltaSBall = normalize(ballDirection);

	ballPosX = ballPosX + deltaSBall.x*deltaTime*ballSpeed;
	ballPosY = ballPosY + deltaSBall.y*deltaTime*ballSpeed;

}

void render()
{
	glClear( GL_COLOR_BUFFER_BIT );
	
	glUseProgram(programID);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	
	glUniform1i(gameStateLocation, currentGameState);

	drawShape(BACKGROUND, vec2(0.0, 0.0));

	
	uint nHexX = resX/hexRadius + 3;
	uint nHexY = 2*resY/hexRadius + 4;

	for(uint i=0; i<nHexX; i++)
		for(uint j=0; j<nHexY; j++)
		{
			vec2 transform = vec2( double(3.0*i*hexRadius)/resX +double((j % 2)*hexRadius*1.5)/resX - 1.0, 
								   0.5*sqrt(3.0)*double(j*(hexRadius))/resY -1.0  );
			// cout<<transform.x<<" "<<transform.y<<"\n";
			drawShape(HEXLINE, transform);
		}

	for(int i=0; i<5; i++)
	{
		int totalLen = (blockWidth)* numberBlocks[i];

		for(int j=0; j<numberBlocks[i]; j++)
		{
			if(gameState[j][i]>=0)
			{
				vec2 transform = vec2((-double(totalLen) + blockWidth + j*2*blockWidth + resX )/resX - 1.0, 
								double(2.0*startingHeight + 2*blockHeight*i)/resY - 1.0);

				drawShape(i, transform);
			}
		}
	}

	// drawShape(BLOCK3, vec2(0.0, 0.0));
	drawShape(BOARDBORDERS, vec2(0.0, 0.0));


	vec2 ballTransform = vec2( 2.0*ballPosX/resX - 1.0, 2.0*ballPosY/resY - 1.0 );
	drawShape(BALL, ballTransform);

	vec2 paddleTransform = vec2( 2.0*paddlePositionX/resX - 1.0, 2.0*paddlePositionY/resY - 1.0 );
	// cout<<paddleTransform.x<<" "<<paddleTransform.y<<"\n";
	drawShape(PADDLE, paddleTransform);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);


	glfwSwapBuffers(window);
}

void initColissions()
{
	for(int i=0; i<6; i++)
	{
		vec2 v1= rotateZ(vec2(boardHexRadius, 0.0), (i+1)*M_PI/3.0);
		boardHexCoords[i] = v1 + vec2(0.5*resX, 0.5*resY);
	}
}

void initShapeBuffer()
{
	shapeBufferSize = 0;
	colorBufferSize = 0;


	double w = double(blockWidth)/resX;
	double h = double(blockHeight)/resY;

	shapeIndexStart[BLOCK0] = shapeBufferSize;
	colorIndexStart[BLOCK0] = colorBufferSize;

	vec2 v[4];
	v[0] = vec2(-w, -h);
	v[1] = vec2(w, -h);
	v[2] = vec2(w, h);
	v[3] = vec2(-w, h);

	double scaleDiffX = cornerRadius/double(blockWidth);
	double scaleDiffY = cornerRadius/double(blockHeight);

	for(int j=0; j<2; j++)
	{
		double sDiffX = (j == 1)*scaleDiffX;
		double sDiffY = (j == 0)*scaleDiffY;

		for(int i=0; i<3; i++)
		{
			shapeBuffer[shapeBufferSize + 2*i] = v[i].x * (1.0 - sDiffX);
			shapeBuffer[shapeBufferSize + 2*i + 1] = v[i].y * (1.0 - sDiffY);

			shapeBuffer[shapeBufferSize + 6 + 2*i] = v[(i + 2) % 4].x * (1.0 - sDiffX);
			shapeBuffer[shapeBufferSize + 6 + 2*i + 1] = v[(i + 2) % 4].y * (1.0 - sDiffY);
		}
		shapeBufferSize += 12;
	}

	for(int j=0; j<4; j++)
		for(int i=0; i<6; i++)
		{
			double radX = double(cornerRadius)/resX;

			// vec2 v1= vec2(radX, 0.0);
			vec2 v1= rotateZ(vec2(radX, 0.0), M_PI_4*(j+2)*2 + (i+1)*M_PI_4/3.0)*vec2(1.0, double(resX)/resY);
			vec2 v2= rotateZ(vec2(radX, 0.0), M_PI_4*(j+2)*2 + (i)*M_PI_4/3.0)*vec2(1.0, double(resX)/resY);

			vec2 tr = v[j]*vec2(1.0 - scaleDiffX, 1.0 - scaleDiffY);
			// vec2 tr = v[j] - vec2(0.5, 0.5);

			shapeBuffer[shapeBufferSize++] = (v1 + tr).x;
			shapeBuffer[shapeBufferSize++] = (v1 + tr).y;

			shapeBuffer[shapeBufferSize++] = (v2 + tr).x;
			shapeBuffer[shapeBufferSize++] = (v2 + tr).y;

			shapeBuffer[shapeBufferSize++] = tr.x;
			shapeBuffer[shapeBufferSize++] = tr.y;
		}

	shapeSize[BLOCK0] = shapeBufferSize - shapeIndexStart[BLOCK0];

	for(int i=0; i<shapeSize[BLOCK0]/2; i++)
	{
		colorBuffer[colorBufferSize++] = blockOutlineColor.x;
		colorBuffer[colorBufferSize++] = blockOutlineColor.y;
		colorBuffer[colorBufferSize++] = blockOutlineColor.z;
	}


	for(int i=0; i<shapeSize[BLOCK0]; i+=2)
	{
		shapeBuffer[shapeBufferSize++] = shapeBuffer[i + shapeIndexStart[BLOCK0]]*(1.0 - (4.0/blockWidth));
		shapeBuffer[shapeBufferSize++] = shapeBuffer[i + 1 + shapeIndexStart[BLOCK0]]*(1.0 - (4.0/blockHeight));
	}

	for(int i=0; i<shapeSize[BLOCK0]/2; i++)
	{
		colorBuffer[colorBufferSize++] = blockColors[BLOCK0].x;
		colorBuffer[colorBufferSize++] = blockColors[BLOCK0].y;
		colorBuffer[colorBufferSize++] = blockColors[BLOCK0].z;
	}

	shapeSize[BLOCK0] = shapeBufferSize - shapeIndexStart[BLOCK0];
	colorSize[BLOCK0] = colorBufferSize - colorIndexStart[BLOCK0];


	for(int blocks = 1; blocks<5; blocks++)
	{	
		shapeIndexStart[blocks] = shapeBufferSize;
		colorIndexStart[blocks] = colorBufferSize;
		for(int j=0; j<shapeSize[BLOCK0]; j+=2)
		{
			shapeBuffer[shapeBufferSize++] = shapeBuffer[j + shapeIndexStart[BLOCK0]];
			shapeBuffer[shapeBufferSize++] = shapeBuffer[j + 1 + shapeIndexStart[BLOCK0]];

			if( j < shapeSize[BLOCK0]/2)
			{
				colorBuffer[colorBufferSize++] = blockOutlineColor.x;
				colorBuffer[colorBufferSize++] = blockOutlineColor.y;
				colorBuffer[colorBufferSize++] = blockOutlineColor.z;
			}
			else
			{
				colorBuffer[colorBufferSize++] = blockColors[blocks].x;
				colorBuffer[colorBufferSize++] = blockColors[blocks].y;
				colorBuffer[colorBufferSize++] = blockColors[blocks].z;
			}
		}

		shapeSize[blocks] = shapeBufferSize - shapeIndexStart[blocks];
		colorSize[blocks] = colorBufferSize - colorIndexStart[blocks];
	}


	shapeIndexStart[HEXLINE] = shapeBufferSize;
	colorIndexStart[HEXLINE] = colorBufferSize;

	for(int i=0; i<7; i++)
	{
		double radX = double(hexRadius)/resX;
		vec2 v1= rotateZ(vec2(radX, 0.0), (i+1)*M_PI/3.0)*vec2(1.0, double(resX)/resY);

		shapeBuffer[shapeBufferSize++] = v1.x;
		shapeBuffer[shapeBufferSize++] = v1.y;

		colorBuffer[colorBufferSize++] = backgroundLineColor.x;
		colorBuffer[colorBufferSize++] = backgroundLineColor.y;
		colorBuffer[colorBufferSize++] = backgroundLineColor.z;
	}
	shapeSize[HEXLINE] = shapeBufferSize - shapeIndexStart[HEXLINE];
	colorSize[HEXLINE] = colorBufferSize - colorIndexStart[HEXLINE];

	shapeIndexStart[BOARDBORDERS] = shapeBufferSize;
	colorIndexStart[BOARDBORDERS] = colorBufferSize;

	vec2 vs[3];
	// vs[0] = vec2(-boardHexRadius, 0.0);
	// vs[1] = vec2(-boardHexRadius*sqrt(2.0), -boardHexRadius*sqrt(2.0));
	// vs[2] = vec2(0.0, -boardHexRadius);

	vs[0] = vec2(-double(2*boardHexRadius)/resX, 0.0);
	vs[1] = vec2(-double(2.0*boardHexRadius)/resX, -sqrt(3.0)*double(boardHexRadius)/resY);
	vs[2] = vec2(-double(boardHexRadius)/resX, -sqrt(3.0)*double(boardHexRadius)/resY);

	double pmX[] = {1.0, -1.0, -1.0, 1.0};
	double pmY[] = {1.0, 1.0, -1.0, -1.0};

	double dX[] = {-1.0, -1.0, 1.0, 1.0};
	double dY[] = {1.0, -1.0, -1.0, 1.0};


	double ddX[] = {-double(2.0*boardHexRadius)/resX, 1.0, double(2.0*boardHexRadius)/resX, -1.0};
	double ddY[] = {-1.0, -sqrt(3.0)*double(boardHexRadius)/resY, 1.0, sqrt(3.0)*double(boardHexRadius)/resY};

	for(int j=0; j<4; j++)
	{
		for(int i=0; i<3; i++)
		{
			shapeBuffer[shapeBufferSize++] = vs[i].x*pmX[j];
			shapeBuffer[shapeBufferSize++] = vs[i].y*pmY[j];

			colorBuffer[colorBufferSize++] = boardBoardersColor.x;
			colorBuffer[colorBufferSize++] = boardBoardersColor.y;
			colorBuffer[colorBufferSize++] = boardBoardersColor.z;
		}
		vec2 tri[2][3];

		tri[0][0] = vec2(dX[j], dY[j]);
		tri[0][1] = vec2(dX[(j+1) % 4], dY[(j+1) % 4]);
		tri[0][2] = 

		tri[1][0] = vec2(ddX[j], ddY[j]);
		tri[1][1] = vec2(ddX[j]*( j % 2 ? -1.0 : 1.0), ddY[j]*( j % 2 ? 1.0 : -1.0));
		tri[1][2] = vec2(dX[j], dY[j]);

		for(int k=0; k<2; k++)
			for(int i=0; i<3; i++)
			{	
				shapeBuffer[shapeBufferSize++] = tri[k][i].x;
				shapeBuffer[shapeBufferSize++] = tri[k][i].y;
				
				colorBuffer[colorBufferSize++] = boardBoardersColor.x;
				colorBuffer[colorBufferSize++] = boardBoardersColor.y;
				colorBuffer[colorBufferSize++] = boardBoardersColor.z;
			}
	}

	shapeSize[BOARDBORDERS] = shapeBufferSize - shapeIndexStart[BOARDBORDERS];
	colorSize[BOARDBORDERS] = colorBufferSize - colorIndexStart[BOARDBORDERS];

	shapeIndexStart[BALL] = shapeBufferSize;
	colorIndexStart[BALL] = colorBufferSize;

	double ballOut[] = {1.0, ballOutline};
	vec3 cols[2];
	cols[0] = blockOutlineColor;
	cols[1] = ballColor;

	for(int k=0; k<2; k++)
		for(int i=0; i<ballSteps; i++)
		{
			double radX = (double(ballRadius)*ballOut[k])/resX;
			vec2 v1= rotateZ(vec2(radX, 0.0), (i)*2.0*M_PI/(ballSteps))*vec2(1.0, double(resX)/resY);
			vec2 v2= rotateZ(vec2(radX, 0.0), (i+1)*2.0*M_PI/(ballSteps))*vec2(1.0, double(resX)/resY);

			shapeBuffer[shapeBufferSize++] = 0;
			shapeBuffer[shapeBufferSize++] = 0;

			shapeBuffer[shapeBufferSize++] = v1.x;
			shapeBuffer[shapeBufferSize++] = v1.y;

			shapeBuffer[shapeBufferSize++] = v2.x;
			shapeBuffer[shapeBufferSize++] = v2.y;

			for(int j=0; j<3; j++)
			{
				colorBuffer[colorBufferSize++] = cols[k].x;
				colorBuffer[colorBufferSize++] = cols[k].y;
				colorBuffer[colorBufferSize++] = cols[k].z;
			}
		}

	shapeSize[BALL] = shapeBufferSize - shapeIndexStart[BALL];
	colorSize[BALL] = colorBufferSize - colorIndexStart[BALL];

	shapeIndexStart[PADDLE] = shapeBufferSize;
	colorIndexStart[PADDLE] = colorBufferSize;

	v[0] = vec2(-0.5*paddleWidth/resX, -0.5*paddleHeight/resY);
	v[1] = vec2(0.5*paddleWidth/resX, -0.5*paddleHeight/resY);
	v[2] = vec2(0.5*paddleWidth/resX, 0.5*paddleHeight/resY);
	v[3] = vec2(-0.5*paddleWidth/resX, 0.5*paddleHeight/resY);

	for(int j=0; j<2; j++)
	{
		for(int i=0; i<3; i++)
		{
			shapeBuffer[shapeBufferSize + 2*i] = v[i].x;
			shapeBuffer[shapeBufferSize + 2*i + 1] = v[i].y;

			shapeBuffer[shapeBufferSize + 6 + 2*i] = v[(i + 2) % 4].x;
			shapeBuffer[shapeBufferSize + 6 + 2*i + 1] = v[(i + 2) % 4].y;

		}
		shapeBufferSize += 12;

		for(int i=0; i<6;i++)
		{
			colorBuffer[colorBufferSize++] = blockOutlineColor.x;
			colorBuffer[colorBufferSize++] = blockOutlineColor.y;
			colorBuffer[colorBufferSize++] = blockOutlineColor.z;
		}
	}

	const int paddSteps = 10;

	for(int i=0; i<paddSteps; i++)
	{	
		double dX = (double(paddleWidth)/resX);
		double radX = (double(paddleHeight)*0.5)/resX;
		vec2 v1= rotateZ(vec2(radX, 0.0), M_PI_2 + (i)*2.0*M_PI/(paddSteps))*vec2(1.0, double(resX)/resY);
		vec2 v2= rotateZ(vec2(radX, 0.0), M_PI_2 + (i+1)*2.0*M_PI/(paddSteps))*vec2(1.0, double(resX)/resY);

		shapeBuffer[shapeBufferSize++] = 0 - dX/2.0 + (i >= paddSteps/2)*dX;
		shapeBuffer[shapeBufferSize++] = 0;

		shapeBuffer[shapeBufferSize++] = v1.x - dX/2.0 + (i >= paddSteps/2)*dX;
		shapeBuffer[shapeBufferSize++] = v1.y;

		shapeBuffer[shapeBufferSize++] = v2.x - dX/2.0 + (i >= paddSteps/2)*dX;
		shapeBuffer[shapeBufferSize++] = v2.y + double(ballOutline/4.0)/double(resY)*(0);

		for(int j=0; j<3; j++)
		{
			colorBuffer[colorBufferSize++] = blockOutlineColor.x;
			colorBuffer[colorBufferSize++] = blockOutlineColor.y;
			colorBuffer[colorBufferSize++] = blockOutlineColor.z;
		}
	}

	shapeSize[PADDLE] = shapeBufferSize - shapeIndexStart[PADDLE];
	colorSize[PADDLE] = colorBufferSize - colorIndexStart[PADDLE];

	for(int i=0; i<shapeSize[PADDLE]; i+=2)
	{
		shapeBuffer[shapeBufferSize++] = shapeBuffer[i + shapeIndexStart[PADDLE]]*(1.0 - (8.0/paddleWidth));
		shapeBuffer[shapeBufferSize++] = shapeBuffer[i + 1 + shapeIndexStart[PADDLE]]*(1.0 - (8.0/paddleHeight));
	}

	for(int i=0; i<shapeSize[PADDLE]/2; i++)
	{
		colorBuffer[colorBufferSize++] = paddleColor.x;
		colorBuffer[colorBufferSize++] = paddleColor.y;
		colorBuffer[colorBufferSize++] = paddleColor.z;
	}

	shapeSize[PADDLE] = shapeBufferSize - shapeIndexStart[PADDLE];
	colorSize[PADDLE] = colorBufferSize - colorIndexStart[PADDLE];

	shapeIndexStart[BACKGROUND] = shapeBufferSize;
	colorIndexStart[BACKGROUND] = colorBufferSize;

	v[0] = vec2(-1.0, -1.0);
	v[1] = vec2(1.0, -1.0);
	v[2] = vec2(1.0, 1.0);
	v[3] = vec2(-1.0, 1.0);

	for(int j=0; j<2; j++)
	{
		for(int i=0; i<3; i++)
		{
			shapeBuffer[shapeBufferSize + 2*i] = v[i].x;
			shapeBuffer[shapeBufferSize + 2*i + 1] = v[i].y;

			shapeBuffer[shapeBufferSize + 6 + 2*i] = v[(i + 2) % 4].x;
			shapeBuffer[shapeBufferSize + 6 + 2*i + 1] = v[(i + 2) % 4].y;

		}
		shapeBufferSize += 12;

		for(int i=0; i<6;i++)
		{
			colorBuffer[colorBufferSize++] = boardColor.x;
			colorBuffer[colorBufferSize++] = boardColor.y;
			colorBuffer[colorBufferSize++] = boardColor.z;
		}
	}

	shapeSize[BACKGROUND] = shapeBufferSize - shapeIndexStart[BACKGROUND];
	colorSize[BACKGROUND] = colorBufferSize - colorIndexStart[BACKGROUND];
}


void handle_input(bool &doExit)
{
	glfwPollEvents();

	if(glfwGetKey(window, GLFW_KEY_ESCAPE ) == GLFW_PRESS)
		doExit = true;

	if(glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS)
		pressedKeys[GLFW_KEY_LEFT] = true;

	if(glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_RELEASE)
		pressedKeys[GLFW_KEY_LEFT] = false;

	if(glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS)
		pressedKeys[GLFW_KEY_RIGHT] = true;

	if(glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_RELEASE)
		pressedKeys[GLFW_KEY_RIGHT] = false;

	static double spacedPressed = false;
	if(glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_RELEASE) spacedPressed = false;

	if(glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_PRESS && !spacedPressed)
	{
		spacedPressed = true;
		if(currentGameState == 0)
		{
			glfwSetWindowTitle(window, "Arkanoid | Playing | Points: 0");
			currentGameState = 1;
		}

		if(currentGameState > 1)
		{
			reset();
			glfwSetWindowTitle(window, "Arkanoid | New Game");
			currentGameState = 0;
		}

	}

}

int main( void )
{
	srand(time(NULL));

	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow( resX, resY, "Arkanoid | New Game", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glewExperimental = true;
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, 0.0f);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	programID = LoadShaders( "arkanoid_vs.vertexshader", "arkanoid_fs.fragmentshader" );

	transformLocation = glGetUniformLocation(programID, "transformVector");
	scaleLocation = glGetUniformLocation(programID, "scaleVector");
	gameStateLocation = glGetUniformLocation(programID, "gameState");


	// colorLocation = glGetUniformLocation(programID, "vColor");
	// hexCoordLocation = glGetUniformLocation(programID, "hexCoords");

	initShapeBuffer();
	initColissions();


	glGenBuffers(1, &shapeBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, shapeBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*shapeBufferSize, shapeBuffer, GL_STATIC_DRAW);

	glGenBuffers(1, &colorBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, colorBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*colorBufferSize, colorBuffer, GL_STATIC_DRAW);

	glClearColor(boardColor.x, boardColor.y, boardColor.z, 0.0);
	glLineWidth(backgroundLineWidth);

	reset();


	bool doExit = false;
	double currentTime = glfwGetTime();
	double acc = 0.0f;
	
	do
	{	
		acc += glfwGetTime() - currentTime;
		currentTime = glfwGetTime();

		while(acc >= deltaTime)
		{
			handle_input(doExit);
			update(deltaTime);
			acc -= deltaTime;
		}
		render();
		// glfwPollEvents();
	}
	while( !doExit && glfwWindowShouldClose(window) == 0);
	glfwTerminate();

	return 0;
}